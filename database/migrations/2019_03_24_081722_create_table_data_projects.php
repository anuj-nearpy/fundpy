<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDataProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_projects', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('project_type_id')->unsigned();
            $table->string('title');
            $table->string('slug');
            $table->float('min_fund');
            $table->float('req_fund');
            $table->float('max_fund');
            $table->string('min_time');
            $table->string('max_time');
            $table->float('approx_return');
            $table->text('description')->nullable();
            $table->text('detailed_description')->nullable();
            $table->text('faq')->nullable();
            $table->longText('terms_conditions')->nullable();
            $table->integer('is_active')->default(1);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
