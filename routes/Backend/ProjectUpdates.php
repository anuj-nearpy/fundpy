<?php

Route::group([
    "namespace"  => "ProjectUpdates",
], function () {
    /*
     * Admin ProjectUpdates Controller
     */

    // Route for Ajax DataTable
    Route::get("projectupdates/get", "AdminProjectUpdatesController@getTableData")->name("projectupdates.get-list-data");

    Route::resource("projectupdates", "AdminProjectUpdatesController");
});