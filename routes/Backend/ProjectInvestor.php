<?php

Route::group([
    "namespace"  => "ProjectInvestor",
], function () {
    /*
     * Admin ProjectInvestor Controller
     */

    // Route for Ajax DataTable
    Route::get("projectinvestor/get", "AdminProjectInvestorController@getTableData")->name("projectinvestor.get-list-data");

    Route::resource("projectinvestor", "AdminProjectInvestorController");
});