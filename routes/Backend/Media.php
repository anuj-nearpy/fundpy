<?php

Route::group([
    "namespace"  => "Media",
], function () {
    /*
     * Admin Media Controller
     */

    // Route for Ajax DataTable
    Route::get("media/get", "AdminMediaController@getTableData")->name("media.get-list-data");

    Route::resource("media", "AdminMediaController");
});