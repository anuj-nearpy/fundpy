<?php

Route::group([
    "namespace"  => "Contacts",
], function () {
    /*
     * Admin Contacts Controller
     */

    // Route for Ajax DataTable
    Route::get("contacts/get", "AdminContactsController@getTableData")->name("contacts.get-list-data");

    Route::resource("contacts", "AdminContactsController");
});