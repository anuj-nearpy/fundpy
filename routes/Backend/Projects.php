<?php

Route::group([
    "namespace"  => "Projects",
], function () {
    /*
     * Admin Projects Controller
     */

    // Route for Ajax DataTable
    Route::get("projects/get", "AdminProjectsController@getTableData")->name("projects.get-list-data");

    Route::resource("projects", "AdminProjectsController");
});