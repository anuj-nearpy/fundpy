<?php

Route::group([
    "namespace"  => "ProjectView",
], function () {
    /*
     * Admin ProjectView Controller
     */

    // Route for Ajax DataTable
    Route::get("projectview/get", "AdminProjectViewController@getTableData")->name("projectview.get-list-data");

    Route::resource("projectview", "AdminProjectViewController");
});