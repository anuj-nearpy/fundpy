<?php

Route::group([
    "namespace"  => "Comment",
], function () {
    /*
     * Admin Comment Controller
     */

    // Route for Ajax DataTable
    Route::get("comment/get", "AdminCommentController@getTableData")->name("comment.get-list-data");

    Route::resource("comment", "AdminCommentController");
});