<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');
Route::get('page', 'FrontendController@page')->name('page');
Route::any('search/', 'FrontendController@search')->name('search');
Route::any('contact-inquiry/', 'FrontendController@inquiry')->name('inquiry');
Route::any('filter-by-category/{slug}', 'FrontendController@filterByCategory')->name('filter-by-category');
Route::get('/index.html', 'FrontendController@index')->name('index-html');
Route::get('contact-us', 'FrontendController@contactUs')->name('contact-us');
Route::get('project-details/{slug}', 'FrontendController@showProjectDetails')->name('show-project-details');
Route::get('projects', 'FrontendController@allProjects')->name('all-projects');
Route::get('macros', 'FrontendController@macros')->name('macros');
Route::post('add-project-comment', 'FrontendController@addComment')->name('add-project-comment');
Route::get('about-us', 'FrontendController@aboutUs')->name('about-us');
Route::get('privacy-policy', 'FrontendController@privacyPolicy')->name('privacy-policy');
Route::get('terms-and-conditions', 'FrontendController@terms')->name('terms-and-conditions');
Route::get('faq', 'FrontendController@getFaq')->name('faq');
Route::get('how-it-work', 'FrontendController@getWorks')->name('works');

//Route::post('project/user-create', 'FrontendController@storeProject')->name('user-project');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
        
        Route::get('user-project/list', 'ProfileController@listProject')->name('project.list');

        Route::get('invested-project/list', 'ProfileController@investedProjects')->name('project.invested-list');

        Route::get('user-project/{id}/edit', 'ProfileController@edit')->name('project.edit');

        Route::get('user-project/create', 'ProfileController@createNewProject')->name('project.create');
        
        Route::post('user-project/{id}/update', 'ProfileController@updateProject')->name('project.update');

      
        Route::post('user-project/store', 'ProfileController@storeProject')->name('project.store');

        Route::any('project-fund/{id}/invest', 'ProfileController@investFund')->name('project.invest-fund');

    });
});
