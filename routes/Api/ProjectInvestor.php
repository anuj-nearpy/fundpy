<?php
Route::group(['namespace' => 'Api'], function()
{
    Route::get('projectinvestor', 'APIProjectInvestorController@index')->name('projectinvestor.index');
    Route::post('projectinvestor/create', 'APIProjectInvestorController@create')->name('projectinvestor.create');
    Route::post('projectinvestor/edit', 'APIProjectInvestorController@edit')->name('projectinvestor.edit');
    Route::post('projectinvestor/show', 'APIProjectInvestorController@show')->name('projectinvestor.show');
    Route::post('projectinvestor/delete', 'APIProjectInvestorController@delete')->name('projectinvestor.delete');
});
?>