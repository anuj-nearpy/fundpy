<?php
Route::group(['namespace' => 'Api'], function()
{
    Route::get('media', 'APIMediaController@index')->name('media.index');
    Route::post('media/create', 'APIMediaController@create')->name('media.create');
    Route::post('media/edit', 'APIMediaController@edit')->name('media.edit');
    Route::post('media/show', 'APIMediaController@show')->name('media.show');
    Route::post('media/delete', 'APIMediaController@delete')->name('media.delete');
});
?>