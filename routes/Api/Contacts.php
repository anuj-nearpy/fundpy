<?php
Route::group(['namespace' => 'Api'], function()
{
    Route::get('contacts', 'APIContactsController@index')->name('contacts.index');
    Route::post('contacts/create', 'APIContactsController@create')->name('contacts.create');
    Route::post('contacts/edit', 'APIContactsController@edit')->name('contacts.edit');
    Route::post('contacts/show', 'APIContactsController@show')->name('contacts.show');
    Route::post('contacts/delete', 'APIContactsController@delete')->name('contacts.delete');
});
?>