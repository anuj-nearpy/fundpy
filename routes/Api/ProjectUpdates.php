<?php
Route::group(['namespace' => 'Api'], function()
{
    Route::get('projectupdates', 'APIProjectUpdatesController@index')->name('projectupdates.index');
    Route::post('projectupdates/create', 'APIProjectUpdatesController@create')->name('projectupdates.create');
    Route::post('projectupdates/edit', 'APIProjectUpdatesController@edit')->name('projectupdates.edit');
    Route::post('projectupdates/show', 'APIProjectUpdatesController@show')->name('projectupdates.show');
    Route::post('projectupdates/delete', 'APIProjectUpdatesController@delete')->name('projectupdates.delete');
});
?>