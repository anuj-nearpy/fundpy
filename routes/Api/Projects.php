<?php
Route::group(['namespace' => 'Api'], function()
{
    Route::get('projects', 'APIProjectsController@index')->name('projects.index');
    Route::post('projects/create', 'APIProjectsController@create')->name('projects.create');
    Route::post('projects/edit', 'APIProjectsController@edit')->name('projects.edit');
    Route::post('projects/show', 'APIProjectsController@show')->name('projects.show');
    Route::post('projects/delete', 'APIProjectsController@delete')->name('projects.delete');
});
?>