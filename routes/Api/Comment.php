<?php
Route::group(['namespace' => 'Api'], function()
{
    Route::get('comment', 'APICommentController@index')->name('comment.index');
    Route::post('comment/create', 'APICommentController@create')->name('comment.create');
    Route::post('comment/edit', 'APICommentController@edit')->name('comment.edit');
    Route::post('comment/show', 'APICommentController@show')->name('comment.show');
    Route::post('comment/delete', 'APICommentController@delete')->name('comment.delete');
});
?>