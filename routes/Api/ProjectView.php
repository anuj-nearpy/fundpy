<?php
Route::group(['namespace' => 'Api'], function()
{
    Route::get('projectview', 'APIProjectViewController@index')->name('projectview.index');
    Route::post('projectview/create', 'APIProjectViewController@create')->name('projectview.create');
    Route::post('projectview/edit', 'APIProjectViewController@edit')->name('projectview.edit');
    Route::post('projectview/show', 'APIProjectViewController@show')->name('projectview.show');
    Route::post('projectview/delete', 'APIProjectViewController@delete')->name('projectview.delete');
});
?>