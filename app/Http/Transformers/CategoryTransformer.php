<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class CategoryTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "categoryId" => (int) $item->id, "categoryUserId" =>  $item->user_id, "categoryTitle" =>  $item->title, "categorySlug" =>  $item->slug, "categoryIcon" =>  $item->icon, "categoryDescription" =>  $item->description, "categoryStatus" =>  $item->status, "categoryCreatedAt" =>  $item->created_at, "categoryUpdatedAt" =>  $item->updated_at, 
        ];
    }
}