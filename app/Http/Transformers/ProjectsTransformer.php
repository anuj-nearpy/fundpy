<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class ProjectsTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "projectsId" => (int) $item->id, "projectsUserId" =>  $item->user_id, "projectsCategoryId" =>  $item->category_id, "projectsProjectTypeId" =>  $item->project_type_id, "projectsTitle" =>  $item->title, "projectsSlug" =>  $item->slug, "projectsMinFund" =>  $item->min_fund, "projectsReqFund" =>  $item->req_fund, "projectsMaxFund" =>  $item->max_fund, "projectsMinTime" =>  $item->min_time, "projectsMaxTime" =>  $item->max_time, "projectsApproxReturn" =>  $item->approx_return, "projectsDescription" =>  $item->description, "projectsDetailedDescription" =>  $item->detailed_description, "projectsFaq" =>  $item->faq, "projectsTermsConditions" =>  $item->terms_conditions, "projectsIsActive" =>  $item->is_active, "projectsStatus" =>  $item->status, "projectsCreatedAt" =>  $item->created_at, "projectsUpdatedAt" =>  $item->updated_at, 
        ];
    }
}