<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class ContactsTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "contactsId" => (int) $item->id, "contactsContactName" =>  $item->contact_name, "contactsContactEmail" =>  $item->contact_email, "contactsContactSubject" =>  $item->contact_subject, "contactsContactMessage" =>  $item->contact_message, "contactsIsRead" =>  $item->is_read, "contactsStatus" =>  $item->status, "contactsCreatedAt" =>  $item->created_at, "contactsUpdatedAt" =>  $item->updated_at, 
        ];
    }
}