<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class ProjectInvestorTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "projectinvestorId" => (int) $item->id, "projectinvestorUserId" =>  $item->user_id, "projectinvestorProjectId" =>  $item->project_id, "projectinvestorInvestedAmount" =>  $item->invested_amount, "projectinvestorTotalShare" =>  $item->total_share, "projectinvestorShareValue" =>  $item->share_value, "projectinvestorPaymentId" =>  $item->payment_id, "projectinvestorPaymentDetails" =>  $item->payment_details, "projectinvestorIsActive" =>  $item->is_active, "projectinvestorStatus" =>  $item->status, "projectinvestorCreatedAt" =>  $item->created_at, "projectinvestorUpdatedAt" =>  $item->updated_at, 
        ];
    }
}