<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class ProjectViewTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "projectviewId" => (int) $item->id, "projectviewProjectId" =>  $item->project_id, "projectviewUserId" =>  $item->user_id, "projectviewViewCount" =>  $item->view_count, "projectviewLatitude" =>  $item->latitude, "projectviewLongitude" =>  $item->longitude, "projectviewCreatedAt" =>  $item->created_at, "projectviewUpdatedAt" =>  $item->updated_at, 
        ];
    }
}