<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class ProjectUpdatesTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "projectupdatesId" => (int) $item->id, "projectupdatesProjectId" =>  $item->project_id, "projectupdatesTitle" =>  $item->title, "projectupdatesDescription" =>  $item->description, "projectupdatesIsActive" =>  $item->is_active, "projectupdatesStatus" =>  $item->status, "projectupdatesCreatedAt" =>  $item->created_at, "projectupdatesUpdatedAt" =>  $item->updated_at, 
        ];
    }
}