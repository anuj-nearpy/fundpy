<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class CommentTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "commentId" => (int) $item->id, "commentProjectId" =>  $item->project_id, "commentName" =>  $item->name, "commentEmail" =>  $item->email, "commentSubject" =>  $item->subject, "commentDescription" =>  $item->description, "commentProfilePic" =>  $item->profile_pic, "commentIsActive" =>  $item->is_active, "commentStatus" =>  $item->status, "commentCreatedAt" =>  $item->created_at, "commentUpdatedAt" =>  $item->updated_at, 
        ];
    }
}