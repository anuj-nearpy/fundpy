<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class MediaTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "mediaId" => (int) $item->id, "mediaUserId" =>  $item->user_id, "mediaProjectId" =>  $item->project_id, "mediaFileName" =>  $item->file_name, "mediaMediaType" =>  $item->media_type, "mediaExternalLink" =>  $item->external_link, "mediaIsActive" =>  $item->is_active, "mediaStatus" =>  $item->status, "mediaCreatedAt" =>  $item->created_at, "mediaUpdatedAt" =>  $item->updated_at, 
        ];
    }
}