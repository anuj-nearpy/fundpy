<?php
namespace App\Http\Transformers;

use App\Http\Transformers;

class PagesTransformer extends Transformer
{
    /**
     * Transform
     *
     * @param array $data
     * @return array
     */
    public function transform($item)
    {
        if(is_array($item))
        {
            $item = (object)$item;
        }

        return [
            "pagesId" => (int) $item->id, "pagesUserId" =>  $item->user_id, "pagesTitle" =>  $item->title, "pagesSlug" =>  $item->slug, "pagesMetaTitle" =>  $item->meta_title, "pagesMetaKeywords" =>  $item->meta_keywords, "pagesMetaDescription" =>  $item->meta_description, "pagesContent" =>  $item->content, "pagesAdditionalContent" =>  $item->additional_content, "pagesStatus" =>  $item->status, "pagesCreatedAt" =>  $item->created_at, "pagesUpdatedAt" =>  $item->updated_at, 
        ];
    }
}