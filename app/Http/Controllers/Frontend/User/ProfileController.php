<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Repositories\Frontend\Access\User\UserRepository;
use Illuminate\Http\Request;
use App\Models\Projects\Projects;
use App\Repositories\Projects\EloquentProjectsRepository;
use App\Models\Category\Category;
use App\Models\ProjectInvestor\ProjectInvestor;
use App\Models\Access\User\User;


/**
 * Class ProfileController.
 */
class ProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $user;

    public $repository;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
        $this->repository = new EloquentProjectsRepository;
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     */
    public function update(UpdateProfileRequest $request)
    {
        $output = $this->user->updateProfile(access()->id(), $request->all());

        // E-mail address was updated, user has to reconfirm
        if (is_array($output) && $output['email_changed']) {
            access()->logout();

            return redirect()->route('frontend.auth.login')->withFlashInfo(trans('strings.frontend.user.email_changed_notice'));
        }

        return redirect()->route('frontend.user.account')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

    public function listProject(Request $request)
    {
        $projects = Projects::where('user_id', access()->user()->id)->get();

        return view('frontend.project.list')->with([
            'projects' => $projects
        ]);
    }

    public function investedProjects(Request $request)
    {
        $user   =   User::with(['invested_projects', 'invested_projects.project'])->where('id', access()->user()->id)->first();


        return view('frontend.project.invested-list')->with([
            'user' => $user
        ]);
    }

    public function createNewProject(Request $request)
    {
        $categories = Category::pluck('title', 'id')->toArray();
        
        return view('frontend.project.create')->with([
            'categories' => $categories,
            'repository' => $this->repository
        ]);
    }

    public function storeProject(Request $request)
    {
        $input = $request->all();

        array_merge($input, ['is_active' => 0]);

        $this->repository->create($input);
        

        return redirect()->route('frontend.user.project.list');
    }

    public function updateProject($id, Request $request)
    {
        $input = $request->all();
        
        array_merge($input, ['is_active' => 0]);

        $status = $this->repository->update($id, $input);
        

        return redirect()->route('frontend.user.project.list');
    }

    public function edit($id, Request $request)
    {
        $item       = $this->repository->findOrThrowException($id);
        $categories = Category::pluck('title', 'id')->toArray();
        
        return view('frontend.project.edit')->with([
            'item'          => $item,
            'categories'    => $categories,
            'repository'    => $this->repository
        ]);
    }

    public function investFund(Request $request)
    {
        $project = Projects::where('id', $request->get('project_id'))->first();

        $shareValue = $project->req_fund / 100;
        $totalShare = $request->get('invested_amount') / $shareValue;
        
        $investData = [
            'user_id'           => access()->user()->id,
            'project_id'        => $request->get('project_id'),
            "invested_amount"   => $request->get('invested_amount'),
            "total_share"       => $totalShare,
            "share_value"       => $shareValue,
            "payment_id"        => $request->get('stripeToken'),
            "payment_details"   => "Payment made on ". date('m-d-Y H:i:s')
        ];

        ProjectInvestor::create($investData);

        return redirect()->route('frontend.user.project.invested-list');
    }
}
