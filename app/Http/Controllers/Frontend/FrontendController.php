<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Projects\Projects;
use App\Models\Category\Category;
use Illuminate\Http\Request;
use App\Models\Comment\Comment;
use App\Models\Contacts\Contacts;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        //dd(session('locale'));
        return view('frontend.index');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros()
    {
        return view('frontend.macros');
    }

    /**
     * Contact Us
     *
     * @return \Illuminate\View\View
     */
    public function contactUs()
    {
        return view('frontend.contact-us');   
    }

    /**
     * About Us
     *
     * @return \Illuminate\View\View
     */
    public function aboutUs()
    {
        return view('frontend.about-us');   
    }

    public function showProjectDetails($slug)
    {
        if(isset($slug))
        {
            $project = Projects::with('investors', 'viewCount')->where('slug', $slug)->first();

            access()->addProjectView($project->id); 

            if(isset($project) && isset($project->id))
            {
                return view('frontend.project-details')->with([
                    'project' => $project
                ]);   
            }
        }

        return redirect()->route('frontend.index');
    }

    public function addComment(Request $request)
    {
        $data = [
            'project_id'    => $request->get('project_id'),
            'name'          => $request->get('name'),
            'email'         => $request->get('email'),
            'subject'       => $request->get('subject'),
            'description'   => $request->get('description')
        ];

        Comment::create($data);

        return redirect()->back();
    }

    public function storeProject(Request $request)
    {
        $data = [
            'project_id'    => $request->get('project_id'),
            'name'          => $request->get('name'),
            'email'         => $request->get('email'),
            'subject'       => $request->get('subject'),
            'description'   => $request->get('description')
        ];

        Comment::create($data);

        return redirect()->back();
    }

    public function search(Request $request)
    {
        $search = $request->get('search');

        if(strlen(trim($search)) > 0 && trim($search) != '')
        {
            $query = Projects::where('title', 'LIKE', '%' . $search . '%')
            ->orWhere('slug', 'LIKE', '%'. $search. '%')
            ->orWhere('min_fund', 'LIKE', '%'. $search. '%')
            ->orWhere('max_fund', 'LIKE', '%'. $search. '%')
            ->orWhere('approx_return', 'LIKE', '%'. $search. '%')
            ->orWhere('description', 'LIKE', '%'. $search. '%')
            ->orWhere('faq', 'LIKE', '%'. $search. '%');
            
            $projects   = $query->get();
            $categories = Category::pluck('title', 'id')->toArray();

            return view('frontend.project.search')->with([
                'keyword'       => $search,
                'projects'      => $projects,
                'categories'    => $categories
            ]);   
        }

        $projects   = Projects::where('is_active', 1)->get();
        $categories = Category::pluck('title', 'id')->toArray();

        return view('frontend.project.search')->with([
            'keyword'       => $search,
            'projects'      => $projects,
            'categories'    => $categories
        ]); 
    }

    public function filterByCategory($slug, Request $request)
    {
        $category = false;
        $projects = [];

        if(strlen($slug) > 0 )
        {
            $category = Category::where('slug', $slug)->first();
        }
        
        if($category)        
        {
            $projects = Projects::where('category_id', $category->id)->get();
        }
        

        $categories = Category::pluck('title', 'id')->toArray();

        return view('frontend.project.filter-by-category')->with([
            'projects'      => $projects,
            'category'      => $category,
            'categories'    => $categories
        ]); 
    }

    public function inquiry(Request $request)
    {
        $input = $request->all();
        
        if(is_array($input) && count($input))
        {
            $status = Contacts::create($input);

            if($status)
            {
                return redirect()->route('frontend.index');
            }
        }
        return redirect()->route('frontend.index');
    }
    
    public function allProjects(Request $request)
    {   
        $query = Projects::where([
            'is_active' => 1,
            'status'    => 2
        ]);

        if($request->has('most_recent'))
        {
            $query->orderBy('id', 'desc');
        }

        if($request->has('most_old'))
        {
            $query->orderBy('id', 'asc');
        }

        if($request->has('project_status'))
        {
            $query->where('status', $request->get('project_status'));   
        }
        
        if($request->has('project_category'))
        {
            $query->where('category_id', $request->get('project_category'));   
        }
        
        $projects   = $query->get();
        $categories = Category::pluck('title', 'id')->toArray();

        return view('frontend.project.all')->with([
            'projects'      => $projects,
            'categories'    => $categories
        ]);   
    }    

    public function privacyPolicy()
    {
        return view('frontend.privacy-policy');        
    }

    public function page(Request $request)
    {
        $page = null;

        if($request->has('slug'))
        {
            $page = $page = access()->getPageContentBySlug($request->get('slug'));
        }

        if($page)
        {
            return view('frontend.page')->with(['page' => $page]);;        
        }

        return redirect()->route('frontend.index');
    }

    public function terms()
    {
        return view('frontend.terms');
    }

    public function getFaq()
    {
        $page = access()->getPageContentBySlug('faq');
        return view('frontend.faq')->with(['page' => $page]);
    }

    public function getWorks()
    {
        $page = access()->getPageContentBySlug('how-it-works');
        return view('frontend.work')->with(['page' => $page]);
    }
}
