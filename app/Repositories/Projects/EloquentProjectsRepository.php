<?php namespace App\Repositories\Projects;

/**
 * Class EloquentProjectsRepository
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\Projects\Projects;
use App\Repositories\DbRepository;
use App\Exceptions\GeneralException;
use App\Models\Access\User\User;
use App\Models\Category\Category;
use App\Models\Media\Media;

class EloquentProjectsRepository extends DbRepository
{
    /**
     * Projects Model
     *
     * @var Object
     */
    public $model;

    /**
     * Projects Title
     *
     * @var string
     */
    public $moduleTitle = 'Projects';
    
    public $uploadPath = '';

    /**
     * Table Headers
     *
     * @var array
     */
    public $tableHeaders = [
        'id'             => 'Id',
        'username'       => 'Username',
        'category_title' => 'Category',
        'title'           => 'Title',
        'min_fund'        => 'Min Fund',
        'req_fund'        => 'Req Fund',
        'max_fund'        => 'Max Fund',
        'max_time'        => 'Max Time',
        'approx_return'   => 'Approx Return',
        'description'     => 'Description',
        'created_at'      => 'Created At',
        "actions"         => "Actions"
    ];

    /**
     * Table Columns
     *
     * @var array
     */
    public $tableColumns = [
        'id' =>   [
                'data'          => 'id',
                'name'          => 'id',
                'searchable'    => true,
                'sortable'      => true
            ],
		'username' =>   [
                'data'          => 'username',
                'name'          => 'username',
                'searchable'    => true,
                'sortable'      => true
            ],
		'category_title' =>   [
                'data'          => 'category_title',
                'name'          => 'category_title',
                'searchable'    => true,
                'sortable'      => true
            ],
		'title' =>   [
                'data'          => 'title',
                'name'          => 'title',
                'searchable'    => true,
                'sortable'      => true
            ],
		'min_fund' =>   [
                'data'          => 'min_fund',
                'name'          => 'min_fund',
                'searchable'    => true,
                'sortable'      => true
            ],
		'req_fund' =>   [
                'data'          => 'req_fund',
                'name'          => 'req_fund',
                'searchable'    => true,
                'sortable'      => true
            ],
		'max_fund' =>   [
                'data'          => 'max_fund',
                'name'          => 'max_fund',
                'searchable'    => true,
                'sortable'      => true
            ],
		'max_time' =>   [
                'data'          => 'max_time',
                'name'          => 'max_time',
                'searchable'    => true,
                'sortable'      => true
            ],
		'approx_return' =>   [
                'data'          => 'approx_return',
                'name'          => 'approx_return',
                'searchable'    => true,
                'sortable'      => true
            ],
		'description' =>   [
                'data'          => 'description',
                'name'          => 'description',
                'searchable'    => true,
                'sortable'      => true
            ],
		'created_at' =>   [
                'data'          => 'created_at',
                'name'          => 'created_at',
                'searchable'    => true,
                'sortable'      => true
            ],
		'actions' => [
            'data'          => 'actions',
            'name'          => 'actions',
            'searchable'    => false,
            'sortable'      => false
        ]
    ];

    /**
     * Is Admin
     *
     * @var boolean
     */
    protected $isAdmin = false;

    /**
     * Admin Route Prefix
     *
     * @var string
     */
    public $adminRoutePrefix = 'admin';

    /**
     * Client Route Prefix
     *
     * @var string
     */
    public $clientRoutePrefix = 'frontend';

    /**
     * Admin View Prefix
     *
     * @var string
     */
    public $adminViewPrefix = 'backend';

    /**
     * Client View Prefix
     *
     * @var string
     */
    public $clientViewPrefix = 'frontend';

    /**
     * Module Routes
     *
     * @var array
     */
    public $moduleRoutes = [
        'listRoute'     => 'projects.index',
        'createRoute'   => 'projects.create',
        'storeRoute'    => 'projects.store',
        'editRoute'     => 'projects.edit',
        'updateRoute'   => 'projects.update',
        'deleteRoute'   => 'projects.destroy',
        'dataRoute'     => 'projects.get-list-data'
    ];

    /**
     * Module Views
     *
     * @var array
     */
    public $moduleViews = [
        'listView'      => 'projects.index',
        'createView'    => 'projects.create',
        'editView'      => 'projects.edit',
        'deleteView'    => 'projects.destroy',
    ];

    /**
     * Construct
     *
     */
    public function __construct()
    {
        $this->model            = new Projects;
        $this->userModel        = new User;
        $this->categoryModel    = new Category;

        $this->uploadPath       = public_path() . '/uploads/images/';
    }

    /**
     * Create Projects
     *
     * @param array $input
     * @return mixed
     */
    public function create($input)
    {
        $input = $this->prepareInputData($input, true);
        $model = $this->model->create($input);

        if($model)
        {
            if(isset($input['files']) && count($input['files']))
            {
                $this->addMedia($model, $input['files']);
            }

            return $model;
        }

        return false;
    }

    public function addMedia($model, $files)
    {
        $data = [];

        if(isset($files) && count($files))
        {
            foreach($files as $file)
            {
                $file->move($this->uploadPath, $file->getClientOriginalName());

                $data[] = [
                    'project_id' => $model->id,
                    'user_id'    => $model->user_id,
                    'file_name'  => $file->getClientOriginalName(),
                    'media_type' => 1,
                ];
            }
        }

        if(isset($data) && count($data))
        {
            Media::insert($data);
        }

        return true;
    }

    /**
     * Update Projects
     *
     * @param int $id
     * @param array $input
     * @return bool|int|mixed
     */
    public function update($id, $input)
    {
        $model = $this->model->find($id);

        if($model)
        {
            $input  = $this->prepareInputData($input);
            $status =  $model->update($input);

            if($status)
            {
                if(isset($input['files']) && count($input['files']))
                {
                    $this->addMedia($model, $input['files']);
                }
            }
        }

        return false;
    }

    /**
     * Destroy Projects
     *
     * @param int $id
     * @return mixed
     * @throws GeneralException
     */
    public function destroy($id)
    {
        $model = $this->model->find($id);

        if($model)
        {
            return $model->delete();
        }

        return  false;
    }

    /**
     * Get All
     *
     * @param string $orderBy
     * @param string $sort
     * @return mixed
     */
    public function getAll($orderBy = 'id', $sort = 'asc')
    {
        return $this->model->orderBy($orderBy, $sort)->get();
    }

    /**
     * Get by Id
     *
     * @param int $id
     * @return mixed
     */
    public function getById($id = null)
    {
        if($id)
        {
            return $this->model->find($id);
        }

        return false;
    }

    /**
     * Get Table Fields
     *
     * @return array
     */
    public function getTableFields()
    {
        return [
            $this->model->getTable().'.*',
            $this->userModel->getTable().'.name as username',
            $this->categoryModel->getTable().'.title as category_title',
        ];
    }

    /**
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->model->select($this->getTableFields())
        ->leftjoin($this->userModel->getTable(), $this->userModel->getTable().'.id', '=', $this->model->getTable().'.user_id')
        ->leftjoin($this->categoryModel->getTable(), $this->categoryModel->getTable().'.id', '=', $this->model->getTable().'.category_id')->get();
    }

    /**
     * Set Admin
     *
     * @param boolean $isAdmin [description]
     */
    public function setAdmin($isAdmin = false)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Prepare Input Data
     *
     * @param array $input
     * @param bool $isCreate
     * @return array
     */
    public function prepareInputData($input = array(), $isCreate = false)
    {
        if($isCreate)
        {
            $input = array_merge($input, ['user_id' => access()->user()->id]);
        }

        return $input;
    }

    /**
     * Get Table Headers
     *
     * @return string
     */
    public function getTableHeaders()
    {
        if($this->isAdmin)
        {
            return json_encode($this->setTableStructure($this->tableHeaders));
        }

        $clientHeaders = $this->tableHeaders;

        unset($clientHeaders['username']);

        return json_encode($this->setTableStructure($clientHeaders));
    }

    /**
     * Get Table Columns
     *
     * @return string
     */
    public function getTableColumns()
    {
        if($this->isAdmin)
        {
            return json_encode($this->setTableStructure($this->tableColumns));
        }

        $clientColumns = $this->tableColumns;

        unset($clientColumns['username']);

        return json_encode($this->setTableStructure($clientColumns));
    }
}