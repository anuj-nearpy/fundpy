<?php namespace App\Models\Category\Traits\Relationship;

use App\Models\Projects\Projects;

trait Relationship
{
	/**
	 * Many-to-Many relations with Role.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function projects()
	{
	    return $this->hasMany(Projects::class, 'category_id');
	}
}