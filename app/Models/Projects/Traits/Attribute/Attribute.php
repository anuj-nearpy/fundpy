<?php namespace App\Models\Projects\Traits\Attribute;

/**
 * Trait Attribute
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com )
 */

use App\Repositories\Projects\EloquentProjectsRepository;
use App\Models\Media\Media;
use Carbon\Carbon;

trait Attribute
{
    /**
     * @return string
     */
    public function getEditButtonAttribute($routes, $prefix = 'admin', $isAdmin = false)
    {
        $id = $isAdmin ? $this->id : hasher()->encode($this->id);

        return '<a href="'.route($prefix .'.'. $routes->editRoute, $id).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute($routes, $prefix = 'admin')
    {
        return '<a href="'.route($prefix .'.'. $routes->deleteRoute, $this).'"
                data-method="delete"
                data-trans-button-cancel="Cancel"
                data-trans-button-confirm="Delete"
                data-trans-title="Do you want to Delete this Item ?"
                class="btn btn-xs btn-danger"><i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Delete"></i></a>';
    }

    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        $repository = new EloquentProjectsRepository;
        $routes     = $repository->getModuleRoutes();

        return $this->getEditButtonAttribute($routes, $repository->clientRoutePrefix) . $this->getDeleteButtonAttribute($routes, $repository->clientRoutePrefix);
    }

    /**
     * @return string
     */
    public function getAdminActionButtonsAttribute()
    {
        $repository = new EloquentProjectsRepository;
        $routes     = $repository->getModuleRoutes();

        return $this->getEditButtonAttribute($routes, $repository->adminRoutePrefix, true) . $this->getDeleteButtonAttribute($routes, $repository->adminRoutePrefix);
    }

    /**
     * Get FundPercentage Attribute
     * 
     * @return string
     */
    public function getFundPercentageAttribute()
    {
        return '0%';
    }

    /**
     * Get Investor Count
     * 
     * @return integer
     */
    public function getInvestorCountAttribute()
    {
        return '0';
    }

    public function getPublishBeforeAttribute()
    {
        $endDate    = Carbon::now();
        $startDate  = $this->created_at;
        $dateDiff   = $endDate->diffInDays($startDate);

        return $dateDiff;
    }

    /**
     * Get Primary Image Attribute
     * 
     * @return string
     */
    public function getPrimaryImageAttribute()
    {
        $media = Media::where('project_id', $this->id)->first();

        if(isset($media) && isset($media->id))
        {
            return url('uploads/images/'.$media->file_name);
        }

        //return url('uploads/images/default.png');
    }

    public function getSmallDescriptionAttribute()
    {
        return substr($this->description, 0, 300) . strlen($this->description)  >  300 ? '...' : '';
    }

    public function getFundingGoalAttribute()
    {
        return $this->min_fund;
    }

    public function getTotalInvestedAttribute()
    {
        return 0;
    }
}