<?php namespace App\Models\Projects\Traits\Relationship;

use App\Models\Category\Category;
use App\Models\Access\User\User;
use App\Models\Comment\Comment;
use App\Models\ProjectUpdates\ProjectUpdates;
use App\Models\ProjectInvestor\ProjectInvestor;
use App\Models\Media\Media;
use App\Models\ProjectView\ProjectView;

trait Relationship
{
	/**
	 * Many-to-Many relations with Role.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsT
	 */
	public function user()
	{
	    return $this->belongsTo(User::class, 'user_id');
	}

	/**
	 * Many-to-Many relations with Role.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsT
	 */
	public function category()
	{
	    return $this->belongsTo(Category::class, 'category_id');
	}

	/**
	 * Many-to-Many relations with Role.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsT
	 */
	public function comments()
	{
	    return $this->hasMany(Comment::class, 'project_id');
	}

	/**
	 * Many-to-Many relations with Role.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsT
	 */
	public function project_updates()
	{
	    return $this->hasMany(ProjectUpdates::class, 'project_id');
	}

	/**
	 * Many-to-Many relations with Role.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsT
	 */
	public function media()
	{
	    return $this->hasMany(Media::class, 'project_id');
	}

	/**
	 * Has Many relations with Investor
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\hasMany
	 */
	public function investors()
	{
	    return $this->hasMany(ProjectInvestor::class, 'project_id');
	}

	/**
	 * HasMany relations with View Count
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function viewCount()
	{
	    return $this->hasMany(ProjectView::class, 'project_id');
	}
}