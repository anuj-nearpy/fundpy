<?php namespace App\Models\Projects;

/**
 * Class Projects
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\BaseModel;
use App\Models\Projects\Traits\Attribute\Attribute;
use App\Models\Projects\Traits\Relationship\Relationship;

class Projects extends BaseModel
{
    use Attribute, Relationship;
    /**
     * Database Table
     *
     */
    protected $table = "data_projects";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        "id", "user_id", "category_id", "project_type_id", "title", "slug", "min_fund", "req_fund", "max_fund", "min_time", "max_time", "approx_return", "description", "detailed_description", "faq", "terms_conditions", "is_active", "status", "created_at", "updated_at", 
    ];

    /**
     * Timestamp flag
     *
     */
    public $timestamps = true;

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}