<?php namespace App\Models\ProjectView;

/**
 * Class ProjectView
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\BaseModel;
use App\Models\ProjectView\Traits\Attribute\Attribute;
use App\Models\ProjectView\Traits\Relationship\Relationship;

class ProjectView extends BaseModel
{
    use Attribute, Relationship;
    /**
     * Database Table
     *
     */
    protected $table = "data_project_views";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        "id", "project_id", "user_id", "view_count", "latitude", "longitude", "created_at", "updated_at", 
    ];

    /**
     * Timestamp flag
     *
     */
    public $timestamps = true;

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}