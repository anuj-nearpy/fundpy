<?php namespace App\Models\Comment;

/**
 * Class Comment
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\BaseModel;
use App\Models\Comment\Traits\Attribute\Attribute;
use App\Models\Comment\Traits\Relationship\Relationship;

class Comment extends BaseModel
{
    use Attribute, Relationship;
    /**
     * Database Table
     *
     */
    protected $table = "data_comments";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        "id", "project_id", "name", "email", "subject", "description", "profile_pic", "is_active", "status", "created_at", "updated_at", 
    ];

    /**
     * Timestamp flag
     *
     */
    public $timestamps = true;

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}