<?php namespace App\Models\ProjectInvestor;

/**
 * Class ProjectInvestor
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\BaseModel;
use App\Models\ProjectInvestor\Traits\Attribute\Attribute;
use App\Models\ProjectInvestor\Traits\Relationship\Relationship;

class ProjectInvestor extends BaseModel
{
    use Attribute, Relationship;
    /**
     * Database Table
     *
     */
    protected $table = "data_project_investors";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        "id", "user_id", "project_id", "invested_amount", "total_share", "share_value", "payment_id", "payment_details", "is_active", "status", "created_at", "updated_at", 
    ];

    /**
     * Timestamp flag
     *
     */
    public $timestamps = true;

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}