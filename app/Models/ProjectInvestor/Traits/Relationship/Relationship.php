<?php namespace App\Models\ProjectInvestor\Traits\Relationship;

use App\Models\Projects\Projects;
use App\Models\Access\User\User;

trait Relationship
{
	/**
	 * Many-to-Many relations with Role.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function user()
	{
	    return $this->belongsTo(User::class, 'user_id');
	}

	/**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function project()
    {
        return $this->belongsTo(Projects::class, 'project_id');
    }
}