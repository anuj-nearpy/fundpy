<?php namespace App\Models\ProjectUpdates;

/**
 * Class ProjectUpdates
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\BaseModel;
use App\Models\ProjectUpdates\Traits\Attribute\Attribute;
use App\Models\ProjectUpdates\Traits\Relationship\Relationship;

class ProjectUpdates extends BaseModel
{
    use Attribute, Relationship;
    /**
     * Database Table
     *
     */
    protected $table = "data_project_updates";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        "id", "project_id", "title", "description", "is_active", "status", "created_at", "updated_at", 
    ];

    /**
     * Timestamp flag
     *
     */
    public $timestamps = true;

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}