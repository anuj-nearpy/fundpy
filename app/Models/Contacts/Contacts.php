<?php namespace App\Models\Contacts;

/**
 * Class Contacts
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\BaseModel;
use App\Models\Contacts\Traits\Attribute\Attribute;
use App\Models\Contacts\Traits\Relationship\Relationship;

class Contacts extends BaseModel
{
    use Attribute, Relationship;
    /**
     * Database Table
     *
     */
    protected $table = "data_contact_inquiries";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        "id", "contact_name", "contact_email", "contact_subject", "contact_message", "is_read", "status", "created_at", "updated_at", 
    ];

    /**
     * Timestamp flag
     *
     */
    public $timestamps = true;

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}