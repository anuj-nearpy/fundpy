<?php namespace App\Models\Media;

/**
 * Class Media
 *
 * @author Anuj Jaha ( er.anujjaha@gmail.com)
 */

use App\Models\BaseModel;
use App\Models\Media\Traits\Attribute\Attribute;
use App\Models\Media\Traits\Relationship\Relationship;

class Media extends BaseModel
{
    use Attribute, Relationship;
    /**
     * Database Table
     *
     */
    protected $table = "data_media_files";

    /**
     * Fillable Database Fields
     *
     */
    protected $fillable = [
        "id", "user_id", "project_id", "file_name", "media_type", "external_link", "is_active", "status", "created_at", "updated_at", 
    ];

    /**
     * Timestamp flag
     *
     */
    public $timestamps = true;

    /**
     * Guarded ID Column
     *
     */
    protected $guarded = ["id"];
}