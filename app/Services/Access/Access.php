<?php

namespace App\Services\Access;

use Illuminate\Contracts\Auth\Authenticatable;
use App\Models\Projects\Projects;
use App\Models\Category\Category;
use App\Models\Pages\Pages;
use App\Models\ProjectView\ProjectView;

/**
 * Class Access.
 */
class Access
{
    /**
     * Get the currently authenticated user or null.
     */
    public function user()
    {
        return auth()->user();
    }

    /**
     * Return if the current session user is a guest or not.
     *
     * @return mixed
     */
    public function guest()
    {
        return auth()->guest();
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        return auth()->logout();
    }

    /**
     * Get the currently authenticated user's id.
     *
     * @return mixed
     */
    public function id()
    {
        return auth()->id();
    }

    /**
     * @param Authenticatable $user
     * @param bool            $remember
     */
    public function login(Authenticatable $user, $remember = false)
    {
        return auth()->login($user, $remember);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function loginUsingId($id)
    {
        return auth()->loginUsingId($id);
    }

    /**
     * Checks if the current user has a Role by its name or id.
     *
     * @param string $role Role name.
     *
     * @return bool
     */
    public function hasRole($role)
    {
        if ($user = $this->user()) {
            return $user->hasRole($role);
        }

        return false;
    }

    /**
     * Checks if the user has either one or more, or all of an array of roles.
     *
     * @param  $roles
     * @param bool $needsAll
     *
     * @return bool
     */
    public function hasRoles($roles, $needsAll = false)
    {
        if ($user = $this->user()) {
            return $user->hasRoles($roles, $needsAll);
        }

        return false;
    }

    /**
     * Check if the current user has a permission by its name or id.
     *
     * @param string $permission Permission name or id.
     *
     * @return bool
     */
    public function allow($permission)
    {
        if ($user = $this->user()) {
            return $user->allow($permission);
        }

        return false;
    }

    /**
     * Check an array of permissions and whether or not all are required to continue.
     *
     * @param  $permissions
     * @param  $needsAll
     *
     * @return bool
     */
    public function allowMultiple($permissions, $needsAll = false)
    {
        if ($user = $this->user()) {
            return $user->allowMultiple($permissions, $needsAll);
        }

        return false;
    }

    /**
     * @param  $permission
     *
     * @return bool
     */
    public function hasPermission($permission)
    {
        return $this->allow($permission);
    }

    /**
     * @param  $permissions
     * @param  $needsAll
     *
     * @return bool
     */
    public function hasPermissions($permissions, $needsAll = false)
    {
        return $this->allowMultiple($permissions, $needsAll);
    }

    /**
     * Get All Projects
     * 
     * @return object
     */
    public function getAllProjects()
    {
        return Projects::with('category')->orderBy('id', 'desc')->get();
    }

    /**
     * Get All Categories
     * 
     * @return object
     */
    public function getAllCategories()
    {
        return Category::with(['projects', 'projects.user'])->orderBy('id', 'desc')->get();
    }

    public function getNextProject($projectId = null)
    {
        $nextId = $projectId + 1;
        $nextProject = Projects::where('id', $nextId)->first();

        if(isset($nextProject))
        {
            return $nextProject;
        }

        return false;
    }

    public function getPreviousProject($projectId = null)
    {
        $nextId = $projectId - 1;
        $nextProject = Projects::where('id', $nextId)->first();

        if(isset($nextProject))
        {
            return $nextProject;
        }

        return false;
    }

    public function getRecentProjects($limit = 5)
    {
        return Projects::limit($limit)->orderBy('id', 'desc')->get();        
    }

    public function myProjects($userId = null)
    {
        return Projects::where('user_id', $userId)->get();        
    }

    public function getProjectStatus() 
    {
        return [
            1 => 'Pending',
            2 => 'Active',
            3 => 'Completed',
            4 => 'Cancel',
            5 => 'Rejected'
        ];
    }

    /**
     * Get Page Content By Slug
     * 
     * @param string $slug
     * @return string
     */
    public function getPageContentBySlug($slug = null)
    {
        if($slug)
        {
            return Pages::where('slug', $slug)->first();
        }

        return '';
    }

    public function addProjectView($projectId = null)
    {
        if($projectId)
        {
            $viewData = [
                'project_id' => $projectId,
                'user_id'    => $this->user() ? $this->user()->id : null,
                'view_count' => 1
            ];

            return ProjectView::create($viewData);
        }

        return true;
    }
}
