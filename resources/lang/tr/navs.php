<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'          => 'Anassayfa',
        'projects'      => 'Projeler',
        'about_us'      => 'Hakımızda',
        'contact_us'    => 'İletişim',
        'call_now'      => 'Şimdi Ara',
        'login_register' => 'Giriş/Üye Ol',
        'create_a_project'    => 'Proje Oluştur',
        'account'       => 'Hesabım',
        'dashboard'       => 'Kontrol Panel',
        'my_projects'       => 'Projelerim',
        'my_investments'       => 'Yatırımlarım',
        'logout'        => 'Logout',
    ],

    'frontend' => [
        'dashboard' => 'Kontrol Panel',
        'login'     => 'Giriş',
        'macros'    => 'Makrolar',
        'register'  => 'Üye Ol',

        'user' => [
            'account'         => 'Hesabım',
            'administration'  => 'Yönetim',
            'change_password' => 'Parola Değiştir',
            'my_information'  => 'Bilgilerim',
            'profile'         => 'Profilim',
        ],
    ],
];
