<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home'          => 'Home',
        'projects'      => 'Projects',
        'about_us'      => 'About Us',
        'contact_us'    => 'Contact Us',
        'call_now'      => 'Call Now',
        'login_register' => 'Login/Register',
        'create_a_project'    => 'Create a Project',
        'account'       => 'Account',
        'dashboard'       => 'Dashboard',
        'my_projects'       => 'My Projects',
        'my_investments'       => 'My Investments',
        'logout'        => 'Logout',
    ],

    'frontend' => [
        'dashboard' => 'Dashboard',
        'login'     => 'Login',
        'macros'    => 'Macros',
        'register'  => 'Register',

        'user' => [
            'account'         => 'My Account',
            'administration'  => 'Administration',
            'change_password' => 'Change Password',
            'my_information'  => 'My Information',
            'profile'         => 'Profile',
        ],
    ],
];
