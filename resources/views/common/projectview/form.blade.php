<div class="box-body">
    <div class="form-group">
        {{ Form::label('project_id', 'Project Id :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('project_id', null, ['class' => 'form-control', 'placeholder' => 'Project Id', 'required' => 'required']) }}
        </div>
    </div>
</div><div class="box-body">
    <div class="form-group">
        {{ Form::label('user_id', 'User Id :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('user_id', null, ['class' => 'form-control', 'placeholder' => 'User Id', 'required' => 'required']) }}
        </div>
    </div>
</div><div class="box-body">
    <div class="form-group">
        {{ Form::label('view_count', 'View Count :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('view_count', null, ['class' => 'form-control', 'placeholder' => 'View Count', 'required' => 'required']) }}
        </div>
    </div>
</div><div class="box-body">
    <div class="form-group">
        {{ Form::label('latitude', 'Latitude :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('latitude', null, ['class' => 'form-control', 'placeholder' => 'Latitude', 'required' => 'required']) }}
        </div>
    </div>
</div><div class="box-body">
    <div class="form-group">
        {{ Form::label('longitude', 'Longitude :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('longitude', null, ['class' => 'form-control', 'placeholder' => 'Longitude', 'required' => 'required']) }}
        </div>
    </div>
</div>