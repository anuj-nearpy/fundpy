<div class="box-body">
    <div class="form-group">
        {{ Form::label('user_id', 'User :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::select('user_id', ['' => "Please Select User"] + $users, null, ['class' => 'form-control', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('category_id', 'Category :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::select('category_id', ['' => 'Please Select Category'] + $categories, null, ['class' => 'form-control', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('status', 'Project Status :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::select('status', ['' => 'Please Select Status'] + [
                1 => 'Pending',
                2 => 'Active',
                3 => 'Completed',
                4 => 'Cancel',
                5 => 'Rejected'
            ], null, ['class' => 'form-control', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('title', 'Title :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('slug', 'Slug :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('min_fund', 'Min Fund :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('min_fund', null, ['class' => 'form-control', 'min' => 0, 'step' => 1, 'placeholder' => 'Min Fund', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('req_fund', 'Req Fund :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('req_fund', null, ['class' => 'form-control', 'min' => 0, 'step' => 1, 'placeholder' => 'Req Fund', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('max_fund', 'Max Fund :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('max_fund', null, ['class' => 'form-control', 'min' => 0, 'step' => 1, 'placeholder' => 'Max Fund', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('min_time', 'Min Time :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('min_time', null, ['class' => 'form-control', 'placeholder' => 'Min Time', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('max_time', 'Max Time :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::text('max_time', null, ['class' => 'form-control', 'placeholder' => 'Max Time', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('approx_return', 'Approx Return :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::number('approx_return', null, ['min' => 0, 'step' => 1, 'class' => 'form-control', 'placeholder' => 'Approx Return', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('description', 'Description :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('detailed_description', 'Detailed Description :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('detailed_description', null, ['class' => 'form-control', 'placeholder' => 'Detailed Description', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('faq', 'Faq :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('faq', null, ['class' => 'form-control', 'placeholder' => 'Faq', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('terms_conditions', 'Terms Conditions :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('terms_conditions', null, ['class' => 'form-control', 'placeholder' => 'Terms Conditions', 'required' => 'required']) }}
        </div>
    </div>
</div>

<div class="box-body">
    <div class="form-group">
        {{ Form::label('files', 'Upload Images :', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            <input type="file" name="files[]" multiple>
        </div>
    </div>
</div>

@if(isset($item->id) && count($item->media))
    <div class="row">
        @foreach($item->media as $media)
            <div class="col-md-3 text-center">
                <img style="width: 90px; height: 100px;" src="{!! url('uploads/images/'. $media->file_name) !!}">
            </div>
        @endforeach
    </ul>   
@endif

