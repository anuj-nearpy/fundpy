<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <a href="javascript:void(0);" target="_blank">{{ trans('strings.backend.general.boilerplate_link') }}</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} <a href="javascript:void(0);">{{ app_name() }}</a>.</strong> {{ trans('strings.backend.general.all_rights_reserved') }}
    <input type="hidden" id="lat" name="lat">
    <input type="hidden" id="long" name="long">
    <script>
    	getLocation();
    	function getLocation() {
    	  if (navigator.geolocation) {
    	    navigator.geolocation.getCurrentPosition(function(position)
    	    {
    	    	if(position)
    	    	{
    	    		document.getElementById("lat").value = position.coords.latitude;
    	    		document.getElementById("lat").value = position.coords.longitude;
    	    	}
    	    });
    	  } else { 
    	    x.innerHTML = "Geolocation is not supported by this browser.";
    	  }
    	}
    </script>
</footer>