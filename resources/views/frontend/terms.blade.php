@extends('frontend.layouts.app')

@section('content')
    <body class="home home-top">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                <div class="page-title background-page">
                    <div class="container">
                        <h1>Say Hello!</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                                <li>Terms</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="page-content contact-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 main-content">
                                <div class="entry-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <center><h2>Terms and Conditions</h2></center>
                                            Welcome to Fundpy!
By using/visiting fundpy.com, you agree to our Terms of Use and Privacy Policy. We provide our website (“site”) and services (“services”) subject to your compliance with our Terms of Use Agreement (“agreement’) and Privacy Policy. This agreement also governs the relationship between Fundpy and you, the Site visitor (including visitors that simply browse the website without becoming a registered user) and/or member (“you”, “your”), with respect to your use of our site. If you don’t agree to our Terms of Use or Privacy Policy, then don’t use or access (or continue to use or access) this site or any of the services that we offer.
1.  Modifications
We reserve the sole right and discretion to modify our Terms of Use or Privacy Policy at any time. Modifications are effective immediately after we give notice of them, which we may do by revising the Date of Last Revision of our Terms of Use or Privacy Policy on the site, or by otherwise posting on the site, or by email or conventional mail, or by any other means which provides reasonable notice.
It is your responsibility to regularly check the site to determine if there have been changes to the Terms of Use and Privacy Policy and to review any changes. You agree that Fundpy is not liable to you for any delay or damages that might result from any changes to the Terms of Use or Privacy Policy, if any.
2.  Registering an Account
You must be 18 years old or over to create an account on Fundpy. Users between the ages of 13 and 17 can use the services with the consent and supervision of a parent or legal guardian who is at least 18 years old, provided such parent or legal guardian agrees to be bound by our Terms of Use and Privacy Policy, and agrees to be responsible for such use of our site and services.
Although browsing our website is an option without registering for an account, you will need to register an account with a username, password, and email address to be associated with an account/to validate an account, to access some of our site functions. You are at all times responsible for your account and all of the activity on it. You also are responsible for keeping your password confidential. If you discover that your account has been used by someone without your permission, you should immediately report it to admin@fundpy.com.
3.  Fundpy is a Venue
Fundpy is a crowdfunding and trading venue for individuals and entities that are getting funds for their own projects, and/or to make contributions to the projects of others. When a project owner creates their project on Fundpy, they’re inviting others to invest in through our platform. Anyone who invests in a project on our site accepts the project owner’s terms of use as well as to our own terms of use. There will be two main contracts for any project funded on our platform; one contract is between us and the project’s owner and the second one is between us and investors. 
Fundpy does its best to assure that the project is going well and as agreed on and the project owner is the sole responsible in case there is any stop or misfortune. Fundpy is not responsible for problems occurred in realizing projects but does its best to help project’s owner to overcome them. 
Investors cannot be able to withdraw their money by selling their shares during establishing regulatory issues of the projects after reaching their final goal and they will be able to sell their shares after finishing regulatory issues. 
We have the highest priority after the project’s owner to buy the investors’ shares and in case no one of us and the project’s owner wants to buy them, so our platform shows theirs shares to other investors to buy them.
4.  License to use Fundpy
Fundpy grants each user a limited, non-exclusive, non-transferable, revocable license (permission) to use our site and services subject to each user’s eligibility and continued compliance with our Terms of Use and Privacy Policy.
5.  Fees
It’s free to create an account on Fundpy. When someone invests in your project, we get 5% of the funding goal besides the payment partners get their fees from using their payment gateways. Project owners will have an opportunity to review and accept our fees before starting their projects. Once project owners start their projects, project owners are agreeing to the fees. 
Our standard crowdfunding fee charge is 5% in addition to any fees from our payment processors, and in addition to any optional service fees (like video production or private coaching) that the project owner may sign up for. Each payment provider is its own company, and Fundpy is not responsible for its performance. Typically, payment processor fees are between 3-5%, but that can vary, and Fundpy is not responsible for payment processor fees. By accessing our third party payment providers, you do so at your own risk – when you invest or create a project, you’re also agreeing to the payment processor’s terms of service, and any of their additional fees.
6.  Taxes
All of us (Fundpy, project owners and investors) are responsible for any taxes associated with receipt of any income get from these projects.
7.  Deleting your Account
You can delete/terminate your account at any time when you don’t have any contribution or you don’t have any project, however, If you have any investment in projects which they are in establishing regulatory issues and you would like to delete your account, you should wait till finish this stage and then we will offer your shares to be sold. After you selling your shares, you can delete and terminate your account. Project owners also couldn’t delete their account until finding a new owner to the project. Generally, if you would like to delete your account, please email admin@fundpy.com and we will take care of it ASAP.


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .container -->
                </div><!-- .page-content -->
                <div class="maps">
                   
                </div><!-- .maps -->
            </main><!-- .site-main -->

            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        
    </body>
@endsection