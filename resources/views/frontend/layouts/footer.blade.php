<footer id="footer" class="site-footer">
                <div class="footer-menu">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-sm-4 col-4">
                                <div class="footer-menu-item">
                                    <h3>Our company</h3>
                                    <ul>
                                        <li><a href="#">What is Startup Idea</a></li>
                                        <li><a href="{!! route('frontend.page', ['slug' => 'project-pricing']) !!}">Project Pricing</a></li>
                                        <li><a href="{!! route('frontend.about-us') !!}">About us</a></li>
                                        <li><a href="{!! route('frontend.works') !!}">How It Works</a></li>
                                        <li><a href="#">What Is This</a></li>
                                        <li><a href="{!! route('frontend.faq') !!}">FAQ</a></li>
                                        <li><a href="{!! route('frontend.privacy-policy') !!}">Privacy Policy</a></li>
                                        <li><a href="{!! route('frontend.terms-and-conditions') !!}">Terms and Conditions</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-4">
                                <div class="footer-menu-item">
                                    <h3>Recent Projects</h3>
                                    <ul>
                                        @php
                                            $projects = access()->getRecentProjects();
                                        @endphp

                                        @if(isset($projects) && count($projects))
                                            @foreach($projects as $project)
                                                <li>
                                                    <a href="{!! route('frontend.show-project-details', ['slug' => $project->slug ]) !!}">
                                                        {!! $project->title !!}
                                                    </a>
                                                </li>
                                            @endforeach
                                        @endif  
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-12 col-12">
                                <div class="footer-menu-item newsletter">
                                    <h3>Newsletter</h3>
                                    <div class="newsletter-description">Private, secure, spam-free</div>
                                    <form action="s" method="POST" id="newsletterForm">
                                        <input type="text" value="" name="s" placeholder="Enter your email..." />
                                        <button type="submit" value=""><span class="ion-android-drafts"></span></button>
                                    </form>
                                    <div class="follow">
                                        <h3>Follow us</h3>
                                        <ul>
                                            <li class="facebook"><a target="_Blank" href="http://www.facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li class="twitter"><a target="_Blank" href="http://www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li class="instagram"><a target="_Blank" href="http://www.instagram.com"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                            <li class="google"><a target="_Blank" href="http://www.google.com"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li class="youtube"><a target="_Blank" href="http://www.youtube.com"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer-top">
                            <p class="payment-img"><img src="images/assets/payment-methods.png" alt=""></p>
                         <div class="footer-top-right">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <a href="{!! route('select-language', ['lang' => 'tr']) !!}">
                                    <img class="lang {!! session('locale') == 'tr' ? 'active' : '' !!}" style="width: 40px; height: 25px;" src="{!! url('lang/tr.png') !!}">
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <a href="{!! route('select-language', ['lang' => 'en']) !!}">
                                    <img  class="lang {!! session('locale') == 'en' ? 'active' : '' !!}" style="width: 40px; height: 25px;" src="{!! url('lang/en.png') !!}">
                                    </a>
                                </div>
                            </div>
                        </div>
                               <!--    <div class="dropdow field-select">
                                    <select name="s">
                                        <option value="">$ US Dollar (USD)</option>
                                        <option value="">£ Pound Sterling (GBP)</option>
                                        <option value="">€ Euro (EUR)</option>
                                    </select>
                                </div>
                                <div class="dropdow field-select">
                                    <select name="s">
                                        <option value="">English</option>
                                        <option value="">Greek (Ελληνικά)</option>
                                        <option value="">Deutsch (German)</option>
                                        <option value="">العربية (Arabic)</option>
                                    </select>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div><!-- .footer-menu -->
                <div class="footer-copyright">
                    <div class="container">
                        <p class="copyright">{!! date('Y') !!} by FundPy. All Rights Reserved.</p>
                        <a href="#" class="back-top">Back to top<span class="ion-android-arrow-up"></span></a>
                    </div>
                </div>
                 <input type="hidden" id="lat" name="lat">
    <input type="hidden" id="long" name="long">
    <script>
        getLocation();
        function getLocation() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position)
            {
                if(position)
                {
                    document.getElementById("lat").value = position.coords.latitude;
                    document.getElementById("lat").value = position.coords.longitude;
                }
            });
          } else { 
            x.innerHTML = "Geolocation is not supported by this browser.";
          }
        }
    </script>
            </footer><!-- site-footer -->