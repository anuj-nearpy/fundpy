<header id="header" class="site-header">
<div class="top-header clearfix">
<div class="container">
<ul class="socials-top">
    <li><a target="_Blank" href="http://www.facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
    <li><a target="_Blank" href="http://www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
    <li><a target="_Blank" href="http://www.google.com"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
    <li><a target="_Blank" href="http://www.linkedin.com"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
    <li><a target="_Blank" href="http://www.youtube.com"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
</ul>
<div class="phone">Call Now +(974) 33 43 2827</div>
<div class="phone">
    <a  class="mr10"  href="{!! route('select-language', ['lang' => 'tr']) !!}">
    <img class="lang {!! session('locale') == 'tr' ? 'active' : '' !!}" style="width: 40px; height: 25px;" src="{!! url('lang/tr.png') !!}">
    </a>
</div>
<div class="phone">
    <a class="mr10" href="{!! route('select-language', ['lang' => 'en']) !!}">
    <img  class="lang {!! session('locale') == 'en' ? 'active' : '' !!}" style="width: 40px; height: 25px;" src="{!! url('lang/en.png') !!}">
    </a>
</div>
</div>
</div>
<div class="content-header">
<div class="container">
<div class="site-brand">
    <a href="{!! route('frontend.index') !!}">
            <img style="width: 150px; height:38px;"  src="{!! url('images/assets/logo.png') !!}" alt="">
    </a>
</div><!-- .site-brand -->
<div class="right-header">                  
    <nav class="main-menu">
        <button class="c-hamburger c-hamburger--htx"><span></span></button>
        <ul>
            <li>
                <a href="{!! url('/') !!}">
                {{ trans('navs.general.home') }}
                <i class="fa fa-caret-down" aria-hidden="true"></i></a>
            </li>
            <li>
                <a href="{!! route('frontend.all-projects') !!}">
                    {{ trans('navs.general.projects') }}
                <i class="fa fa-caret-down" aria-hidden="true"></i></a>
            </li>
            <li>
                    <a href="{!! route('frontend.about-us') !!}">
                        {{ trans('navs.general.about_us') }}
                    <i class="fa fa-caret-down" aria-hidden="true"></i></a>
            </li>
            <li>
                    <a href="{!! route('frontend.contact-us') !!}">
                    {{ trans('navs.general.contact_us') }}
                    <i class="fa fa-caret-down" aria-hidden="true"></i></a>
            </li>

            @if(access()->user())   
                <li>
                    <a href="{{ route('frontend.user.project.create') }}">
                        {{ trans('navs.general.create_a_project') }}
                    <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                </li>
                <li>
                <a href="#">{{ trans('navs.general.account') }}
                <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                    <ul class="sub-menu">
                        <li><a href="{!! route('frontend.user.account') !!}">
                        {{ trans('navs.general.dashboard') }}
                        </a></li>
                        <li><a href="{!! route('frontend.user.project.list') !!}">
                                {{ trans('navs.general.my_projects') }}
                            </a>
                        </li>
                        <li><a href="{!! route('frontend.user.project.invested-list') !!}">
                        {{ trans('navs.general.my_investments') }}</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </nav><!-- .main-menu -->
    <div class="search-icon">
        <a href="#" class="ion-ios-search-strong"></a>
        <div class="form-search"></div>
        <form action="{!! route('frontend.search') !!}" method="GET" id="searchForm">
            <input type="text" value="" name="search" placeholder="Search..." />
            <button type="submit" value=""><span class="ion-ios-search-strong"></span></button>
        </form>
    </div>  

    @if(access()->user() == null)
    <div class="login login-button">
        <a href="{{ route('frontend.auth.login') }}" class="btn-primary">
            {{ trans('navs.general.login_register') }}
        </a>
    </div>
    @else
    <div class="login login-button">
        Welcome, {!! access()->user()->name !!}
        <a href="{{ route('frontend.auth.logout') }}" class="btn-primary">Logout</a>
    </div>
    @endif
</div><!--. right-header -->
</div><!-- .container -->
</div>
</header><!-- .site-header -->