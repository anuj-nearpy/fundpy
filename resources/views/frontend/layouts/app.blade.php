<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Application')">
        <meta name="author" content="@yield('meta_author', 'Application')">
        @yield('meta')

        <!-- Styles -->
        @yield('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        @langRTL
            {{ Html::style(getRtlCss(mix('css/frontend.css'))) }}
        @else
            {{ Html::style(mix('css/frontend.css')) }}
        @endif

        <link type="text/css" href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}" />
        <!--<link rel="stylesheet" type="text/css" href="style.css" />
        <link rel="stylesheet" type="text/css" href="css/responsive.css" />-->

        @yield('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body id="app-layout">
        <div id="app">
            @include('includes.partials.logged-in-as')
            {{--@include('frontend.includes.nav')--}}

            <div class="outer-container">
                @include('includes.partials.messages')
                @yield('content')
            </div><!-- container -->
        </div><!--#app-->

        <!-- Scripts -->
        @yield('before-scripts')
        {!! Html::script(mix('js/frontend.js')) !!}
        @yield('after-scripts')

        <script src="{!! asset('js/jquery-1.12.4.min.js') !!}"></script>
        <script src="{!! asset('css/libs/popper/popper.js') !!}></script>
        <script src="{!! asset('css/libs/bootstrap/js/bootstrap.min.js') !!}></script>
        <script src="{!! asset('css/libs/owl-carousel/owl.carousel.min.js') !!}"></script>
        <script src="{!! asset('css/libs/owl-carousel/carousel.min.js') !!}"></script>
        <script src="{!! asset('css/libs/jquery.countdown/jquery.countdown.min.js') !!}"></script>
        <script src="{!! asset('css/libs/wow/wow.min.js') !!}"></script>
        <script src="{!! asset('css/libs/isotope/isotope.pkgd.min.js') !!}"></script>
        <script src="{!! asset('css/libs/bxslider/jquery.bxslider.min.js') !!}"></script>
        <script src="{!! asset('css/libs/magicsuggest/magicsuggest-min.js') !!}"></script>
        <script src="{!! asset('css/libs/quilljs/js/quill.core.js') !!}"></script>
        <script src="{!! asset('css/libs/quilljs/js/quill.js') !!}"></script>
        <!-- orther script -->
        <script src="{!! asset('js/main.js') !!}"></script>

        <script type="text/javascript" src="{!! asset('js/custom/custom.js') !!}"></script>


        @include('includes.partials.ga')
    </body>
</html>