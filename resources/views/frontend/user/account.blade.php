@extends('frontend.layouts.app')

@section('content')
    <body class="home home-top">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')


            <main id="main" class="site-main">
                <div class="page-title background-page">
                    <div class="container">
                        <h1>Dashboard</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.user.account') !!}">Home</a><span>/</span></li>
                                <li>Dashboard</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="account-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3">
                                <nav class="account-bar">
                                    <ul>
                                        <li class="active">
                                            <a href="{!! route('frontend.user.account') !!}">
                                                Dashboard
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{!! route('frontend.user.project.list') !!}">
                                                My Projects
                                            </a>
                                            </li>
                                        <li>
                                            <a href="{!! route('frontend.user.project.invested-list') !!}">
                                                My Investement
                                            </a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-lg-9">
                                <div class="account-content dashboard">
                                    <h3 class="account-title">Dashboard</h3>
                                    <div class="account-main">
                                        <div class="author clearfix">
                                            <a class="author-avatar" href="#">
                                                <img src="{!! url('uploads/user/'.  $logged_in_user->profile_pic ) !!}" alt="">
                                                </a>
                                            <div class="author-content">
                                                <div class="author-title"><h3><a href="#">
                                                    {{ $logged_in_user->name }}
                                                </a>
                                                    </h3>
                                                        <!--<a class="edit-profile" href="#">Edit Profile</a>-->
                                                    </div>
                                                <div class="author-info">
                                                    <p>
                                                        {{ $logged_in_user->email }}
                                                    </p>
                                                    <p>Register Member since 
                                                    {{ date('M Y', strtotime($logged_in_user->created_at)) }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dashboard-latest">
                                            <h3>My Projects</h3>
                                            @php
                                                $projects = access()->myProjects($logged_in_user->id);
                                            @endphp
                                            <ul>
                                            @if(isset($projects) && count($projects))
                                                @foreach($projects as $project)
                                                    <li>
                                                        <a href="#">
                                                            <img src="images/my-campaigns-01.jpg" alt="">
                                                        </a>
                                                        <div class="dashboard-latest-box">
                                                            <div class="category">
                                                                <a href="javascript:void(0);">
                                                                    {!! $project->category->title !!}               
                                                                </a>
                                                            </div>
                                                            <h4>
                                                                <a href="{!! route('frontend.show-project-details', ['slug' => $project->slug ]) !!}">
                                                                {!! $project->name !!}
                                                                </a>
                                                            </h4>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            @else
                                                <span class="alert alert-warning">
                                                    No Projects Found !
                                                </span>
                                            @endif
                                        </div>
                                        <div class="payment-info">
                                            <p>Payment Info:</p>
                                            <p>Paypal Receiver Email: 
                                            <a href="#"></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .container -->
                </div><!-- .account-content -->
            </main><!-- .site-main -->
           

            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        <!-- jQuery -->    
       
           
    </body>
@endsection