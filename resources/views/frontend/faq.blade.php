@extends('frontend.layouts.app')

@section('content')
    <body class="home home-top">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                <div class="page-title background-page">
                    <div class="container">
                        <h1>FAQ</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                                <li>FAQ</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="page-content contact-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 main-content">
                                <div class="entry-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <center><h2>FQA</h2></center>
                                            {!! $page->content !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .container -->
                </div><!-- .page-content -->
                
            </main><!-- .site-main -->

            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        <!-- jQuery -->    
    </body>
@endsection