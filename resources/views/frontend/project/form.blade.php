<div class="field">
    <label for="project_title">Project Title</label>
    <span class="label-desc">Our search looks through words from your project title and blurb, so make them clear and descriptive of what you’re making. Your profile name will be searchable, too.</span>
    {{ Form::text('title', isset($item) && isset($item->id) ? $item->title : null, ['class' => '', 'placeholder' => 'Title', 'required' => 'required']) }}
</div>

<div class="field">
    <span class="label-desc">Select Best fit Category</span>
    {{ Form::select('category_id', ['' => 'Please Select Category'] + $categories, isset($item) && isset($item->id) ? $item->category_id : null, ['class' => '', 'required' => 'required']) }}
</div>

<div class="field">
    <label for="slug">Slug</label>
    <span class="label-desc">Enter URL Friendly Title, which help to get more users.</span>
    {{ Form::text('slug', isset($item) && isset($item->id) ? $item->slug : null, ['class' => '', 'placeholder' => 'Slug', 'required' => 'required']) }}
</div>

<div class="field">
    <label for="min_fund">Minimum Fund</label>
    <span class="label-desc">Enter minimum fund to start the Project.</span>
    {{ Form::number('min_fund', isset($item) && isset($item->id) ? $item->min_fund : null, ['class' => '', 'min' => 0, 'step' => 1, 'placeholder' => 'Min Fund', 'required' => 'required']) }}
</div>

<div class="field">
    <label for="req_fund">Require Fund</label>
    <span class="label-desc">Enter require fund to start the Project.</span>
    {{ Form::number('req_fund', isset($item) && isset($item->id) ? $item->req_fund : null, ['class' => '', 'min' => 0, 'step' => 1, 'placeholder' => 'Req Fund', 'required' => 'required']) }}
</div>

<div class="field">
    <label for="max_fund">Max Fund</label>
    <span class="label-desc">Enter max fund that help to complete the Project.</span>
    {{ Form::number('max_fund', isset($item) && isset($item->id) ? $item->max_fund : null, ['class' => '', 'min' => 0, 'step' => 1, 'placeholder' => 'Max Fund', 'required' => 'required']) }}
</div>


<div class="field">
    <label for="min_time">Minimum Time</label>
    <span class="label-desc">Enter minimum time that require to create Prototype.</span>
    {{ Form::text('min_time', isset($item) && isset($item->id) ? $item->min_time : null, ['class' => '', 'placeholder' => 'Min Time', 'required' => 'required']) }}
</div>


<div class="field">
    <label for="max_time">Maximum Time</label>
    <span class="label-desc">Enter maximum time that require to create Prototype.</span>
        {{ Form::text('max_time', isset($item) && isset($item->id) ? $item->max_time :  null, ['class' => '', 'placeholder' => 'Max Time', 'required' => 'required']) }}
</div>


<div class="field">
    <label for="approx_return">Approx Return</label>
    <span class="label-desc">Enter approx Return.</span>
        {{ Form::number('approx_return', isset($item) && isset($item->id) ? $item->approx_return :  null, ['min' => 0, 'step' => 1, 'class' => '', 'placeholder' => 'Approx Return', 'required' => 'required']) }}
</div>


<div class="field">
    <label for="description">Project Description</label>
    <span class="label-desc">Use your project description to share more about what you’re raising funds to do and how you plan to pull it off. It’s up to you to make the case for your project.</span>
       {{ Form::textarea('description',isset($item) && isset($item->id) ? $item->description :  null, ['class' => '', 'placeholder' => 'Description', 'required' => 'required']) }}
</div>


<div class="field">
    <label for="detailed_description">Project Detailed Description</label>
    <span class="label-desc">Use your project detailed description to share more about what you’re raising funds to do and how you plan to pull it off. It’s up to you to make the case for your project.</span>
       {{ Form::textarea('detailed_description',isset($item) && isset($item->id) ? $item->detailed_description :  null, ['class' => '', 'placeholder' => 'Detailed Description', 'required' => 'required']) }}
</div>

<div class="field">
    <label for="faq">FAQ</label>
    <span class="label-desc">Provide FAQ</span>
      {{ Form::textarea('faq', isset($item) && isset($item->id) ? $item->faq :  null, ['class' => '', 'placeholder' => 'Faq', 'required' => 'required']) }}
</div>

<div class="field">
    <label for="terms_conditions">Terms And Conditions</label>
    <span class="label-desc">Provide Terms and Conditions</span>
      {{ Form::textarea('terms_conditions', isset($item) && isset($item->id) ? $item->faq :  null, ['class' => '', 'placeholder' => 'Faq', 'required' => 'required']) }}
</div>


<div class="field">
    <label for="files">Upload Media</label>
    <span class="label-desc">Upload All Images/PPT/PDF</span>
       <input type="file" name="files[]" multiple>
</div>


@if(isset($item->id) && count($item->media))
    <div class="row">
        @foreach($item->media as $media)
            <div class="col-md-3 text-center">
                <img style="width: 90px; height: 100px;" src="{!! url('uploads/images/'. $media->file_name) !!}">
            </div>
        @endforeach
    </div>   
@endif


