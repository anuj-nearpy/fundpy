@extends('frontend.layouts.app')

@section('content')
    <body class="campaign-detail">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                <div class="page-title background-page" style="margin-bottom: 30px;">
                    <div class="container">
                        <h1>All Project List!</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                                <li>Filter By {!! $category->title !!}</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="campaigns-action clearfix">
                    <div class="container">
                            <div class="sort">
                                <span>Sort by:</span>
                                <ul>
                                    <li class="{!! request()->has('most_recent') ? 'active' : '' !!}">
                                        <a href="{!! route('frontend.all-projects', ['most_recent' => 1]) !!}">
                                            Recent Project
                                        </a>
                                    </li>
                                    <li class="{!! request()->has('most_old') ? 'active' : '' !!}">
                                        <a href="{!! route('frontend.all-projects', ['most_old' => 1]) !!}">
                                            Old First
                                        </a>
                                    </li>
                                </ul>   
                            </div><!-- .sort -->
                            <div class="filter">
                                <span>Filter by:</span>
                                <form action="{!! route('frontend.all-projects') !!}">
                                    <div class="field-select">
                                        <select name="project_status">
                                            @php
                                                $projectStatus = access()->getProjectStatus();
                                            @endphp

                                            <option value="">All Stages</option>
                                            @foreach($projectStatus as $key => $status)
                                                <option value="{!!  $key !!}"> {!! $status !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="field-select">
                                        {!! Form::select('project_category', ['' => 'Select Category'] + $categories, null) !!}
                                    </div>

                                    <div class="field-select">
                                        <input type="submit" name="Filter" value="Filter" class="btn btn-sm btn-primary">
                                    </div>
                                </form>
                            </div><!-- .filter -->
                        </div>
                    </div>
                    <div class="campaigns">             
                    <div class="container">
                        <div class="row">
                            @if(isset($projects) && count($projects))
                            @foreach($projects as $project)
                                <div class="col-lg-12">
                                    <div class="campaign-big-item clearfix">
                                    <a href="{!! route('frontend.show-project-details', ['slug' => $project->slug]) !!}" class="campaign-big-image">

                                    <img src="{!! $project->primaryImage !!}" style="padding: 20px; height: 300px !important;" alt=""></a>
                                        <div class="campaign-big-box">
                                            <a href="{!! route('frontend.filter-by-category', ['slug' => $project->category->slug ]) !!}" class="category">
                                                {!! $project->category->title !!}
                                            </a>
                                        <h3>
                                            <a href="{!! route('frontend.show-project-details', ['slug' => $project->slug])  !!}">
                                            {!! $project->title !!}
                                            </a>


                                            
                                           
                                            

                                        </h3>
                                        <div class="campaign-description">
                                            {!! $project->smallDescription !!}

                                        </div>
                                        <div class="staff-picks-author">
                                            <div class="author-profile">
                                                <a class="author-avatar" href="#">
                                                    <img src="{!! url('uploads/user/'.$project->user->profile_pic) !!}" alt="">
                                                    </a>by
                                                <a class="author-name" href="javascript:void(0);">
                                                    {!! $project->user->name !!}
                                                </a>
                                            </div>

                                            <div class="author-address mr20">
                                                @if(access()->user())
                                                    @if($project->investors->where('user_id', access()->user()->id)->first())
                                                        Already invested
                                                    @else
                                                       <a class="btn btn-sm btn-primary" href="{!! route('frontend.show-project-details', ['slug' => $project->slug]) !!}">
                                                            Invest Now
                                                        </a>
                                                    @endif
                                                @endif
                                            </div>

                                            <div class="author-address mr20"><span class="ion-location"></span>
                                                {!! $project->user->address !!}
                                            </div>
                                        </div>
                                        <div class="process" style="width: 80% !important;">
                                            <div class="raised"><span style="width: 248px;"></span></div>
                                            <div class="process-info">
                                                <div class="process-pledged"><span>
                                                    {!! $project->fundingGoal !!}
                                                </span>Funding Goal</div>
                                                <div class="process-funded"><span>
                                                {!! $project->totalInvested !!}
                                                </span> Invested</div>
                                                <div class="process-time"><span>
                                                    {!! $project->investorCount !!}

                                                </span> investors</div>
                                                <div class="process-time"><span>
                                                {!! $project->publishBefore !!}
                                                </span>days ago</div>

                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>     
                            @endforeach           
                            @else
                                <div class="col-md-12">
                                    <p class="label label-warning">No Projects Found!</p>
                                    <div class="clearfix"></div>
                                    <a  style="margin-top: 20px;" href="{{ route('frontend.user.project.create') }}" class="btn-primary">Create New</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    </div>

                </div><!-- .page-content -->
            </main><!-- .site-main -->


            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        
    </body>
@endsection