@extends('frontend.layouts.app')

@section('content')
    <body class="campaign-detail">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                <div class="page-title background-page">
                    <div class="container">
                        <h1>My Project List!</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                                <li>Project List</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="page-content contact-content">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-lg-12 main-content">
                                <div class="entry-content">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <nav class="account-bar">
                                                <ul>
                                                    <li>
                                                        <a href="{!! route('frontend.user.account') !!}">
                                                            Dashboard
                                                        </a>
                                                    </li>
                                                    <li  class="active">
                                                        <a href="{!! route('frontend.user.project.list') !!}">
                                                            My Projects
                                                        </a>
                                                        </li>
                                                    <li>
                                                        <a href="{!! route('frontend.user.project.invested-list') !!}">
                                                            My Investement
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>

                                        <div class="col-lg-9">
                                        @if(isset($projects) && count($projects))
                                            <table class="table">
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Category</th>
                                                    <th>Status</th>
                                                    <th>Min Fund Require</th>
                                                    <th>Require Fund</th>
                                                    <th>Max Fund Require</th>
                                                    <th>Require Time</th>
                                                    <th>Approx Return</th>
                                                    <th>Created At</th>
                                                    <th>Action</th>
                                                </tr>

                                                @foreach($projects as $project)
                                                    <tr>
                                                        <td> {!! $project->title !!} </td>
                                                        <td> {!! $project->category->title !!} </td>
                                                        <td> {!! access()->getProjectStatus($project->status)!!} </td>
                                                        <td> {!! $project->min_fund !!} </td>
                                                        <td> {!! $project->req_fund !!} </td>
                                                        <td> {!! $project->max_fund !!} </td>
                                                        <td> {!! $project->max_time !!} </td>
                                                        <td> {!! $project->approx_return !!} </td>
                                                        <td> {!! date('d-m-Y', strtotime($project->created_at)) !!} </td>
                                                        <td>
                                                            <a href="{!! route('frontend.user.project.edit', ['id' => $project->id]) !!}" class="btn btn-success">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        @else
                                            <div class="col-md-12">
                                                <p class="label label-warning">No Projects Found!</p>
                                                <div class="clearfix"></div>
                                                <a  style="margin-top: 20px;" href="{{ route('frontend.user.project.create') }}" class="btn-primary">Create New</a>
                                            </div>
                                        @endif
                                        </div>


                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .container -->
                </div><!-- .page-content -->
            </main><!-- .site-main -->


            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        
    </body>
@endsection