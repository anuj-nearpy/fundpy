@extends ('frontend.layouts.app')

@section ('title', isset($repository->moduleTitle) ? 'Create - '. $repository->moduleTitle : 'Create')

@section('page-header')
    <h1>
        {{ isset($repository->moduleTitle) ? $repository->moduleTitle : '' }}
        <small>Create</small>
    </h1>
@endsection

@section('content')
    <body class="home home-top">
    <div class="preloading">
        <div class="preloader loading">
          <span class="slice"></span>
          <span class="slice"></span>
          <span class="slice"></span>
          <span class="slice"></span>
          <span class="slice"></span>
          <span class="slice"></span>
        </div>
    </div>
    <div id="wrapper">
        @include('frontend.layouts.header-bar')

        <main id="main" class="site-main">
            <div class="page-title background-page">
                <div class="container">
                    <h1>Create New Project</h1>
                    <div class="breadcrumbs">
                        <ul>
                            <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                            <li>Create Project</li>
                        </ul>
                    </div><!-- .breadcrumbs -->
                </div>
            </div><!-- .page-title -->
            <div class="page-content contact-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 main-content">
                            <div class="entry-content">
                                <div class="row">
                                    <div class="col-lg-8 campaign-form form-update">
                                            <form class="form-horizontal" action="{!! route('frontend.user.project.store') !!}" method="POST" 
                                            enctype="multipart/form-data">

                                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">


                                             @include('frontend.project.form')


                                            <div class="pull-right">
                                                {{ Form::submit('Save', ['class' => 'btn btn-success btn-xs']) }}
                                            </div>

                                            {{ Form::close() }}
                                                                        
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="contact-info">
                                            <h3>Contact Infomation</h3>
                                            <ul>
                                                <li><i class="fa fa-map-marker" aria-hidden="true"></i>Doha / Qatar</li>
                                                <li><i class="fa fa-phone" aria-hidden="true"></i> +(974) 33 43 2827</li>
                                                <li><i class="fa fa-mobile" aria-hidden="true"></i> +(974) 33 43 2827</li>
                                                <li><i class="fa fa-envelope-o" aria-hidden="true"></i>info@fundpy.com</li>
                                            </ul>
                                            <div class="contact-desc"><p>IF YOU HAVE ANY QUESTIONS, DO NOT HESITATE TO CONTACT US</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .container -->
            </div><!-- .page-content -->
        </main><!-- .site-main -->
        
        </div><!-- #wrapper -->
        
        
        
        <!-- jQuery -->   
        
    
@endsection


@section('after-scripts')
    
@stop