@extends('frontend.layouts.app')

@section('content')
    <body class="home home-top">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                <div class="page-title background-page">
                    <div class="container">
                        <h1>Say Hello!</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                                <li>Privacy Policy</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="page-content contact-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 main-content">
                                <div class="entry-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <center><h2>Privacy Policy</h2></center>
                                            Fundpy Privacy Policy
                                            Date of last revision: May 1, 2019
                                            By visiting this website, you are accepting the practices described in this privacy policy and you consent to receive it via electronic form via publication on the world wide web between you (“You,” “User,” “Your”) and fundpy.com (“Fundpy”, “the site,” “the service,” “us,” we”).
                                            By doing business with or interacting with Fundpy in the manner described in this Privacy Policy at any time on or after the Effective Date, you are accepting the practices described in this Privacy Policy and you expressly consent to the application of this Privacy Policy to the collection, storage, use and disclosure of all your personal information as described.
                                            If you do not agree to be bound by this agreement, do not access or use any part of this website. Fundpy reserves the right, with or without notice, to make changes to this agreement at Fundpy’s discretion at any time. Continued use of any part of this website constitutes your acceptance of such changes. The most current version of this agreement supersedes all previous versions. We encourage you to check all online policies and similar documents relevant to your situation periodically to keep informed about their status.

                                            1.  What We Collect
                                            In order for you to create an account on Fundpy and use our services, we need to collect and process some information. Depending on your use of the Services, that may include:
                                            o   information (such as your name, email and postal addresses, telephone number, and country of residence) that you provide by completing forms on Fundpy, including if you register as a user of the Services, subscribe to our newsletters, upload or submit any material through Fundpy, or request any information;
                                            o   your login and password details, in connection with the account sign-in process;
                                            o   details of any requests or transactions you make through the Services;
                                            o   information about your activity on and interaction with Fundpy, such as your IP address and the type of device or browser you use;
                                            o   communications you send to us (for example, if you ask for support, send us questions or comments, or report a problem); and
                                            o   information that you submit to Fundpy in the form of comments, contributions to discussions, or messages to other users.
                                             
                                            2.  What we keep private
                                            o   any payment information you provide
                                            o   your password details
                                            o   your IP address
                                            o   your phone number
                                            o   communications you send to us (for example, when you ask for support, send us questions or comments, or report a problem)
                                            We don’t give your personal information to any third-party services, except when it’s necessary to provide Fundpy’s Services (like when we partner with payment processors).
                                            We do reserve the right to disclose personal information when we believe that doing so is reasonably necessary to comply with the law or law enforcement, to prevent fraud or abuse, or to protect Fundpy’s legal rights.
                                             
                                            1.  What’s shared between Investors and Project Owners
                                            o   When you invest in a project, the project owner will know your account name, the amount you have invested, your email address. Owners never receive investors’ credit card details or other payment information.
                                            o   If you have invested in a project, the owners will receive the email address associated with your Fundpy account.  
                                            2.  What is public
                                            o   When you create an account, your profile page on Fundpy, contains your account name, the date the account was created, and a list of projects you have backed or launched. Whenever your account name appears on Fundpy (for instance, when you post comments, send messages, or back projects), people can click your account name to see your profile.
                                            o   Some of the things that will be publicly viewable on your profile (when you set it), or elsewhere on Fundpy include:
                                               the account name, and the date it was created
                                               the information you choose to add to your profile (like a picture, bio, or your location)
                                               campaigns you’ve backed (but not pledge amounts or rewards chosen)
                                               projects you’ve launched
                                               any comments you’ve posted on Fundpy
                                               if you have “Liked” a campaign update
                                               Project owners are also asked to verify their identities before launching a project. Once completed, the project’s owners Verified Name will be publicly displayed on their account profile and on any projects they launch.
                                            1.  We’ll use your personal information to identify you when you sign in to your account;
                                            o   enable us to provide you with our Services;
                                            o   send you marketing communications we think you may find useful, in accordance with your email preferences;
                                            o   present projects to you when you use the Services that we believe will be of interest based on your geographic location and previous use of the Services;
                                            o   administer your account with us;
                                            o   enable us to contact you regarding any question you make through the Services;
                                            o   analyze the use of the Services and the people visiting to improve our content and Services; and
                                            o   use for other purposes that we may disclose to you when we request your information.
                                            We never post anything to your Facebook, Twitter, or other third-party accounts without your permission. Fundpy project owners never receive investors’ credit card details or other payment information. We do not and will not sell your data.
                                             
                                            2.  Email Communication 
                                            We try to keep emails to a minimum and give you the ability to opt-out of any marketing communications we send. We will send you email relating to your personal transactions. You may also occasionally receive certain marketing email communications, in accordance with your preferences, and from which you may opt out at any time. We may also send you service-related announcements when necessary.
                                             
                                            3.  Service Providers 
                                            We may engage certain trusted third parties to perform functions and provide services to us, such as hosting and maintenance, database storage and management, advertising sales, e-mail marketing, e-commerce functions and credit card/payment processing. We will share your personally identifiable information with these third parties to the extent necessary for them to perform these functions and subject to their agreement to maintain the privacy and security of your data.
                                             
                                            4.  Compliance with Laws and Law Enforcement
                                            Our policy is to cooperate with government and law enforcement officials and, in certain circumstances, private parties to enforce and comply with the law. We may disclose your personal information to government or law enforcement officials or private parties to the extent we determine, in our sole discretion, is necessary or appropriate in connection with any actual or prospective legal proceedings (including subpoenas) or to establish, enforce or defend our legal rights and interests or the safety of the public or any person, to prevent or stop any illegal, unethical, or legally questionable activity, or to otherwise comply with applicable court orders, laws, rules and regulations.
                                             
                                            5.  Age restriction:
                                            People under 22 (or the legal age in your jurisdiction) are not permitted to use Fundpy on their own. As such, this privacy policy makes no provision for their use of Fundpy.

                                            1.  Security
                                            Safeguarding the confidentiality and use of your personally identifiable information is very important to us. We employ reasonable administrative and technical measures designed to protect your information from loss, misuse, alteration or unauthorized access.
                                             
                                            2.  Cookies
                                            Like many other websites, we may also use “cookies” to collect additional website usage data to improve the Site and its functionality. A cookie is a small data file that we transfer to your computer’s hard disk. Cookies help us to: (1) speed navigation, keep track of items you are purchasing, and provide you with content that is tailored to you; (2) remember information you gave us so that you don’t have to re-enter it; (3) determine the effectiveness of some of our marketing efforts and communications; and (4) monitor the total number of visitors, pages viewed, and the total number of banners displayed. You can stop accepting cookies by setting your Internet browser accordingly, but this will likely result in our not recognizing you or your preferences on future visits to the Site
                                             
                                            3.  Third parties and links to other websites
                                            Our Site may contain advertisements and links to other websites. This is not an endorsement, authorization or representation of any affiliation with those advertisers or the operators of those websites. We do not exercise control over third-party websites. They may place their own cookies or other files on your computer and collect or solicit both personally identifiable and non-personally identifiable information from you. Other websites have their own privacy policies and practices and we encourage you to carefully read the privacy policies or statements of the other websites and services you visit.
                                             
                                            1.  Misc.:
                                            o   You agree that this Privacy Policy shall be exclusively governed and interpreted as pursuant to the laws of the State of Qatar. You specifically consent to personal jurisdiction of state of Qatar in connection with any dispute between you and Fundpy arising out of this Privacy Policy or pertaining to the subject matter hereof.
                                            o   You agree that Fundpy is deemed a passive website that does not give rise to jurisdiction over Fundpy or its parents, subsidiaries, affiliates, assigns, employees, agents, directors, officers, or shareholders (“et al”), either specific or general, in any jurisdiction other than Qatar State. You expressly waive the right to seek equitable remedies/special damages/injunction against Fundpy et al, as well as a waiver to seek any punitive or consequential damages.
                                            o   You expressly agree to that any and all disputes or claims shall be resolved exclusively by final and binding arbitration, rather than in court, except that a User at its election may assert claims in small claims court in Qatar if the user’s claim qualifies for adjudication in that court. The Federal Arbitration Act governs the interpretation and enforcement of this agreement to arbitrate.
                                            o   To the maximum extent permitted by law, the parties agree to arbitrate claims on an individual basis only, and they waive any right to bring, participate in, or recover under a class, collective, consolidated, or representative action.
                                            o   Any arbitration will be conducted by the American Arbitration Association (AAA) under the rules applicable to the claim asserted (http://www.adr.org) If for any reason a claim proceeds in court rather than or after arbitration, the dispute shall exclusively be brought and heard in a court of competent jurisdiction in the State of New Jersey. Except as otherwise provided by law or the AAA’s rules, the prevailing party in any arbitration will be entitled to receive from the non-prevailing party all of its reasonable attorney’s fees and costs.
                                            o   You agree that no joint venture, partnership, employment, or agency relationship exists between you and Fundpy as a result of this agreement or use of this website.
                                            o   This Privacy Policy constitutes the entire agreement between the parties relating to this subject matter and supersedes all prior or contemporaneous communications and proposals, whether electronic, oral or written between the user and Fundpy with respect to Fundpy’s website.

                                            2.  GDPR FAQ:
                                            What personal data does Fundpy collect and store, and for what purposes? 
                                            Fundpy collects the following data: 
                                            •   Mandatory data: 
                                            Fundpy collects your First Name, Email address, City, State, Postal Code, IP Address. This data is used in communications, customer support, marketing, advertising, and to derive product insights. 
                                            •   Optional data:
                                            Fundpy gives you the option of entering a username, a display name, an avatar photo, and links to your Facebook, Twitter, LInkedIn, Instagram, and personal websites. This data is optional and is not used in any automated processing or profiling. 
                                            May I opt out of Fundpy Communications?
                                            Yes, users may opt-out of communications by either "unsubscribing" from the footer of an email or by filling out this form.
                                            Can I edit my Fundpy Data?
                                            Yes, you can edit your data at any time from your profile page.
                                            Can I delete my Fundpy account? 
                                            Yes, if you wish to delete your account and you don’t have any investments or don’t launch any project, please fill out this form, and our customer happiness team will be glad to do it for you. In case you have investments and want to delete your account, first we open your shares to be sold and once they are sold, your account can be deleted. In case you have launched projects and want to delete your account, you couldn’t delete your account until we have found new owners for your ongoing projects. Please allow 7-10 business days for your account to be deleted.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .container -->
                </div><!-- .page-content -->
                <div class="maps">
                   
                </div><!-- .maps -->
            </main><!-- .site-main -->

            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        <!-- jQuery -->    
       
            <!-- End Google Analytics -->
            <script src="https://maps.googleapis.com/maps/api/js"></script>
            <script>
                // When the window has finished loading create our google map below
                google.maps.event.addDomListener(window, 'load', init);

                function init() {
                    // Basic options for a simple Google Map
                    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                    var mapOptions = {
                        // How zoomed in you want the map to start at (always required)
                        zoom: 15,
                        scrollwheel: false,
                        // The latitude and longitude to center the map (always required)
                        center: new google.maps.LatLng(51.477286, -0.201811), // New York

                        // How you would like to style the map. 
                        // This is where you would paste any style found on Snazzy Maps.
                        styles: [
                                {
                                    "featureType": "all",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "weight": "2.00"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "all",
                                    "elementType": "geometry.stroke",
                                    "stylers": [
                                        {
                                            "color": "#9c9c9c"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "all",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#f2f2f2"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape.man_made",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "saturation": -100
                                        },
                                        {
                                            "lightness": 45
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#eeeeee"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#7b7b7b"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.icon",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#46bcec"
                                        },
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#c8d7d4"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#070707"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                }
                            ]
                    };
                    // Get the HTML DOM element that will contain your map 
                    // We are using a div with id="map" seen below in the <body>
                    var mapElement = document.getElementById('map');

                    // Create the Google Map using our element and options defined above
                    var map = new google.maps.Map(mapElement, mapOptions);

                    // Let's also add a marker while we're at it
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(18.689100, 105.691398),
                        map: map,
                        title: 'Snazzy!'
                    });
                }
            </script>
    </body>
@endsection