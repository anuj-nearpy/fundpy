@extends('frontend.layouts.app')

@section('content')
    <body class="campaign-detail">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                <div class="page-title background-campaign">
                    <div class="container">
                        <h1>{!! $project->title !!}</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                                <li>{!! $project->title !!}</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="campaign-content">
                    <div class="container">
                        <div class="campaign">
                            <div class="campaign-item clearfix">
                                <div class="campaign-image">
                                    <div id="owl-campaign" class="campaign-slider">
                                        @if(isset($project->media) && count($project->media))
                                            @foreach($project->media as $media)
                                                <div class="item">
                                                    <img src="{!! url('uploads/images/'.$media->file_name) !!}" alt="">
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="campaign-box">
                                    <a href="#" class="category">{!! $project->category->title !!}</a>
                                    <h3>{!! $project->title !!}</h3>
                                    <div class="campaign-description"><p>
                                        {!! $project->description !!}
                                    </p></div>
                                    <div class="campaign-author clearfix">
                                        <div class="author-profile">
                                            <a class="author-icon" href="#"><img src="{!! url('uploads/user/'.$project->user->profile_pic) !!}" alt=""></a>by <a class="author-name" href="#">
                                                {!! $project->user->name !!}
                                            </a>
                                        </div>
                                        <div class="author-address"><span class="ion-location"></span>{!! $project->user->address !!}</div>
                                    </div>
                                    <div class="process">
                                        <div class="raised"><span></span></div>
                                        <div class="process-info">
                                            <div class="process-funded"><span>
                                                {!! $project->min_fund !!}
                                            </span>Funding Goal</div>
                                            <div class="process-pledged"><span>
                                                {!! $project->min_fund !!}
                                            </span>pledged</div>
                                            <div class="process-time"><span>
                                            {!! $project->publishBefore !!}
                                            </span>days ago</div>
                                            <div class="process-time"><span>
                                            {!! isset($project->viewCount) ? count($project->viewCount) : 0!!}
                                            </span><span class="fa fa-eye"></div>
                                        </div>
                                    </div>
                                    <div class="button">
                                        @if(access()->user())
                                            @if($project->investors->where('user_id', access()->user()->id)->first()&& 1 == 2)
                                               Already invested in this Project
                                               <br>
                                               <a href="{!! route('frontend.user.project.invested-list') !!}">
                                                    See My Invested Projects 
                                                </a>
                                            @else
                                            <form action="{!! route('frontend.user.project.invest-fund', ['id' => $project->id]) !!}" id="priceForm" class="campaign-price quantity">
                                            
                                            <input type="number" id="total_share" value="100" min="0" name="invested_amount" placeholder="" />
                                                
                                                {{ Form::hidden('stripeToken')}}
                                                {{ Form::hidden('total_amount' , 100, ['id' => 'total_amount']) }}
                                                {{ Form::hidden('project_id' , $project->id) }}

                                                <script
                                                    src="https://checkout.stripe.com/checkout.js"
                                                    class="stripe-button subscription-button"
                                                    data-key="pk_test_sDcDdKYCpU8bdripfDWWwViA00jcifZAQY"
                                                    data-image={{ url('images/assets/logo.png') }}
                                                    data-name="Project-{!! $project->title !!}"
                                                    data-email="{{access()->user()->email}}"
                                                    data-description="Invest In {{$project->title}}"
                                                    data-locale="auto"
                                                    amount="$('total_amount').val()">
                                                </script>
                                            </form>
                                            @endif
                                        @else
                                            Please Login to Invest in {!! $project->title !!}.
                                            <a href="{!! route('frontend.auth.login') !!}" class="btn-secondary">
                                                Login Now
                                            </a>
                                        @endif
                                        <!--<a href="#" class="btn-secondary"><i class="fa fa-heart" aria-hidden="true"></i>Remind me</a>-->
                                    </div>
                                    <div class="share">
                                        <p>Share this project</p>
                                        <ul>
                                            <li class="share-facebook"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                            <li class="share-twitter"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                            <li class="share-google-plus"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                            <li class="share-linkedin"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                            <li class="share-code"><a href="#"><i class="fa fa-code" aria-hidden="true"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .campaign-content -->
                <div class="campaign-history">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="campaign-tabs">
                                    <ul class="tabs-controls">
                                        <li class="active" data-tab="campaign"><a href="#">Description</a></li>
                                        <!--<li data-tab="backer"><a href="#">Backer List</a></li>-->
                                        <li data-tab="faq"><a href="#">FAQ</a></li>
                                        <li data-tab="updates"><a href="#">Updates</a></li>
                                        <li data-tab="comment"><a href="#">Comments</a></li>
                                    </ul>
                                    <div class="campaign-content">
                                        <div id="campaign" class="tabs active">
                                            <p>
                                                {!! $project->detailed_description !!}
                                            </p>
                                        </div>
                                        <!--<div id="backer" class="tabs">
                                            <table>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Donate Amount</th>
                                                    <th>Date</th>
                                                </tr>
                                                
                                            </table>
                                        </div>-->
                                        <div id="faq" class="tabs">
                                            <h2>Frequently Asked Questions</h2>
                                            {!! $project->faq !!}
                                        </div>
                                        <div id="updates" class="tabs">
                                            <ul>
                                                @if(isset($project->project_updates) && count($project->project_updates))

                                                    @foreach($project->project_updates as $update)
                                                        <li>
                                                            <p class="date">
                                                            {!! date('Y-m-d', strtotime($update->created_at)) !!}
                                                            </p>
                                                            <h3>
                                                                {!! $update->title !!}
                                                            </h3>
                                                            <div class="desc"><p>
                                                            {!! $update->description !!}
                                                            </p></div>
                                                        </li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <div id="comment" class="tabs comment-area">
                                            <h3 class="comments-title">{!! isset($project->comments) ? count($project->comments) : 'No '!!} Comments</h3>
                                            <ol class="comments-list">
                                                @if(isset($project->comments) && count($project->comments))
                                                    @foreach($project->comments as $comment)
                                                    <li class="comment clearfix"> 
                                                        <div class="comment-body">
                                                            <div class="comment-avatar"><img src="images/comment.jpg" alt=""></div>
                                                            <div class="comment-info">
                                                                <header class="comment-meta"></header>
                                                                <cite class="comment-author">
                                                                    {!! $comment->name !!}
                                                                </cite>
                                                                <div class="comment-inline">
                                                                    <span class="comment-date">
                                                                        {!! $comment->publishBefore !!}
                                                                    </span>
                                                                </div>
                                                                <div class="comment-content">
                                                                <p>
                                                                    {!! $comment->description !!}
                                                                </p></div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                @endif
                                            </ol>
                                            <div id="respond" class="comment-respond">
                                                <h3 id="reply-title" class="comment-reply-title">Leave A Comment?</h3>
                                                <form action="{!! route('frontend.add-project-comment') !!}" method="post"  id="commentForm">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="row">
                                                        <div class="col-md-4 field">
                                                            <input type="text" value="" name="name" placeholder="Your Name" />
                                                        </div>
                                                        <div class="col-md-4 field">
                                                            <input type="email" value="" name="email" placeholder="Your Email" />
                                                        </div>
                                                        <div class="col-md-4 field">
                                                            <input type="text" value="" name="subject" placeholder="Subject" />
                                                        </div>
                                                    </div>
                                                    <div class="field-textarea">
                                                        <textarea rows="8" name="description" placeholder="Your Comment"></textarea>
                                                    </div>
                                                    <input type="hidden" name="project_id" value="{!! $project->id !!}">
                                                    <button type="submit" value="Send Messager" class="btn-primary">Post Comment</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .main-content -->
                            <div class="col-lg-4">
                                <div class="support support-campaign">
                                    <h3 class="support-campaign-title">Similar Projects</h3>
                                    <div class="plan">
                                        @php
                                            $nextProject = access()->getNextProject($project->id);
                                            $previousProject = access()->getPreviousProject($project->id);
                                        @endphp
                                        @if($nextProject)
                                        <a href="{!! route('frontend.show-project-details', ['slug' => $nextProject->slug]) !!}">
                                            <h4>Pledge {!! $nextProject->min_fund !!} - {!! $nextProject->max_fund !!}</h4>
                                            <div class="plan-desc"><p>
                                                {!! $nextProject->description !!}
                                            </p></div>
                                            <div class="plan-date">{!! date('m, Y', strtotime($nextProject->created_at)) !!}</div>
                                            <div class="plan-author">{!! $nextProject->max_time !!} Estimated Delivery</div>
                                            <!--<div class="backer">2 backer</div>-->
                                        </a>
                                        @endif
                                    </div>
                                    <div class="plan">
                                        @if($previousProject)
                                        <a href="{!! route('frontend.show-project-details', ['slug' => $previousProject->slug]) !!}">
                                            <h4>Pledge {!! $previousProject->min_fund !!} - {!! $previousProject->max_fund !!}</h4>
                                            <div class="plan-desc"><p>
                                                {!! $previousProject->description !!}
                                            </p></div>
                                            <div class="plan-date">{!! date('m, Y', strtotime($previousProject->created_at)) !!}</div>
                                            <div class="plan-author">{!! $previousProject->max_time !!} Estimated Delivery</div>
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div><!-- .sidebar -->
                        </div>
                    </div>
                </div><!-- .campaign-history -->
            </main><!-- .site-main -->

            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        <!-- jQuery -->    
       
    </body>
@endsection


@section('after-scripts')
<script>
    jQuery('#total_share').keyup(function() {

        jQuery('#total_amount').val(jQuery('#total_share').val());
    });
</script>
@endsection