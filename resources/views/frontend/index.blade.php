    @extends('frontend.layouts.app')

@section('content')
    <body class="home home-top">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                @include('frontend.layouts.banner')
                <div class="project-love">
                    <div class="container">
                        <h2 class="title">Projects We Love</h2>
                        <div class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</div>
                        <div class="tab-menu tab-row">
                            @php
                                $categories = access()->getAllCategories();
                                $categorySr = 0;
                             @endphp
                            <ul id="bx-pager" class="menu-category">
                                @if(isset($categories) && count($categories))
                                    @foreach($categories as $category)
                                        @if(isset($category->projects) && count($category->projects))
                                            <li class="mc-option {!! $loop->first ? 'active' : '' !!}" data-tab="pp{!! $categorySr !!}">
                                                <a href="{!! route('frontend.filter-by-category', ['slug' => $category->slug ]) !!}" data-slide-index="{!! $categorySr !!}">
                                                    {!! $category->title !!}
                                                </a>
                                            </li>   
                                            @php $categorySr++; @endphp
                                        @endif
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="popular-project">
                                    @php $popularSr = 0; @endphp
                                    @if(isset($categories) && count($categories))
                                        @foreach($categories as $category)
                                            @if(isset($category->projects) && count($category->projects))
                                                <div id="pp{!! $popularSr !!}" class="pp-item {!! $loop->first ? 'active' : '' !!}">
                                                    <img src="images/assets/book-bookmark.svg" alt="">
                                                    <h4>{!! $category->projects->first()->title !!}</h4>
                                                    <p>
                                                    {!! substr($category->projects->first()->description, 0, 100)  !!}
                                                    </p>
                                                    <a href="{!! route('frontend.show-project-details', ['slug' => $category->projects->first()->slug ]) !!}" class="btn btn-sm btn-primary">View</a>
                                                </div>
                                                @php $popularSr++; @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <ul class="project-love-slider pls-col">
                                    @php $sliderSr = 0; @endphp
                                    @if(isset($categories) && count($categories))
                                        @foreach($categories as $category)
                                            @if(isset($category->projects) && count($category->projects))
                                                <li>
                                                    <div class="project-love-item clearfix">
                                                        <div class="index-image-container">
                                                        <a href="{!! route('frontend.show-project-details', ['slug' => $category->projects->first()->slug ]) !!}" class="project-love-image">
                                                            <img src="{!! $category->projects->first()->primaryImage !!}" alt="">
                                                        </a>
                                                        </div>
                                                        <div class="project-love-item-content project-love-box">
                                                            <a href="{!! route('frontend.show-project-details', ['slug' => $category->projects->first()->slug ]) !!}" class="category">
                                                                {!! $category->title !!}
                                                            </a>
                                                            <h3>
                                                                <a href="{!! route('frontend.show-project-details', ['slug' => $category->projects->first()->slug ]) !!}">{!! $category->projects->first()->title !!}</a>
                                                            </h3>
                                                            <div class="project-love-description">
                                                                {!! $category->projects->first()->description !!}
                                                            </div>
                                                            <div class="project-love-author">
                                                                <div class="author-profile">
                                                                    <a class="author-avatar" href="#">
                                                                    <img src="{!! url('uploads/user/'.$category->projects->first()->user->profile_pic) !!}" alt="">
                                                                    </a>
                                                                    by
                                                                    <a class="author-name" href="#">
                                                                    {!! $category->projects->first()->user->name !!}
                                                                    </a>
                                                                </div>
                                                                <div class="author-address">
                                                                    <span class="ion-location"></span>
                                                                        {!! $category->projects->first()->user->address !!}
                                                                    </div>
                                                            </div>
                                                            <div class="process">
                                                                <div class="raised"><span></span></div>
                                                                <div class="process-info">
                                                                    <div class="process-pledged"><span>${!! $category->projects->first()->req_fund !!}</span>pledged</div>
                                                                    <div class="process-funded"><span>{!! $category->projects->first()->fundPercentage !!}</span>funded</div>
                                                                    <div class="process-time"><span>
                                                                        {!! $category->projects->first()->investorCount !!}
                                                                    </span>backers</div>
                                                                    <div class="process-time"><span>{!! $category->projects->first()->publishBefore !!}</span>days ago</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>

                                                @php $sliderSr++; @endphp
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- .project-love -->
                <div class="how-it-work">
                    <div class="container">
                        <h2 class="title">See How It Work</h2>
                        <div class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="item-work">
                                    <div class="item-icon"><span>01</span><i class="fa fa-flask" aria-hidden="true"></i></div>
                                    <div class="item-content">
                                        <h3 class="item-title">Discover Ideas</h3>
                                        <div class="item-desc"><p>A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.</p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="item-work">
                                    <div class="item-icon"><span>02</span><i class="fa fa-leaf" aria-hidden="true"></i></div>
                                    <div class="item-content">
                                        <h3 class="item-title">Create a Campaigns</h3>
                                        <div class="item-desc"><p>Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues.</p></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="item-work">
                                    <div class="item-icon"><span>03</span><i class="fa fa-money" aria-hidden="true"></i></div>
                                    <div class="item-content">
                                        <h3 class="item-title">Get Funded</h3>
                                        <div class="item-desc"><p>Li lingues differe solmen in li grammatica, li pronunciation e li plu commun vocabules.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .how-it-work -->
                
                <div class="latest campaign">
                    <div class="container">
                        <h2 class="title">Discover Campaigns</h2>
                        <div class="campaign-tabs filter-theme">
                            @if(isset($categories) && count($categories))
                                @foreach($categories as $category)
                                    @php
                                        $title = strtolower(str_replace(" ", "", $category->title));
                                    @endphp
                                    <button class="button {!! $loop->first ? 'is-checked' :'' !!}" data-filter=".{!! $title !!}">
                                        {!! $category->title !!}
                                    </button>
                                @endforeach
                            @endif
                        </div>
                        <div class="campaign-content grid">
                            <div class="row">
                                @if(isset($categories) && count($categories))
                                    @foreach($categories as $category)
                                        @php
                                            $title = strtolower(str_replace(" ", "", $category->title));
                                        @endphp
                                        
                                        @if(isset($category->projects) && count($category->projects))
                                            @foreach($category->projects as $project)
                                                <div class="col-lg-4 col-md-6 col-sm-6 col-6 filterinteresting {!! $title !!} filterpopular">
                                                    <div class="campaign-item">
                                                        <a class="overlay" href="#">
                                                            <img data-id="{!! $project->id  !!}" src="{!! $project->primaryImage !!}" height="370" alt="">
                                                            <span class="ion-ios-search-strong"></span>
                                                        </a>
                                                        <div class="campaign-box">
                                                            <a href="#" class="category">
                                                            {!! $category->title !!}
                                                            </a>
                                                            <h3>
                                                            <a href="{!! route('frontend.show-project-details', ['slug' => $project->slug ]) !!}">

                                                                {!! $project->title !!}
                                                            </a></h3>
                                                            <div class="campaign-description">
                                                                {!! $project->description !!}
                                                            </div>
                                                            <div class="campaign-author"><a class="author-icon" href="#"><img src="{!! url('uploads/user/'.$project->user->profile_pic) !!}" alt=""></a>by <a class="author-name" href="#">
                                                                {!! $project->user->name !!}

                                                            </a></div>
                                                            <div class="process">
                                                                <div class="raised"><span></span></div>
                                                                <div class="process-info">
                                                                    <div class="process-pledged"><span>{!! $project->req_fund !!}</span>pledged</div>
                                                                    <div class="process-funded"><span>
                                                                    {!! $project->fundPercentage !!}
                                                                    73%</span>funded</div>
                                                                    <div class="process-time"><span>
                                                                    {!! $project->publishBefore !!}
                                                                    </span>days ago</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        {{--<div class="latest-button"><a href="#" class="btn-primary">View all Projects</a></div>--}}
                    </div>
                </div><!-- .latest -->

                {{--<div class="blognews">
                    <div class="container">
                        <h2 class="title">From the Journal</h2>
                        <div class="description">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore.</div>
                        <div class="blognews-content">
                            <div class="row">
                                <div class="col-lg-4">
                                    <article class="post">
                                        <div class="blognews-thumb">
                                            <a class="overlay" href="javascript:void(0);">
                                                <img src="images/journal-01.jpg" alt="">
                                                <span class="ion-ios-search-strong"></span>
                                            </a>
                                        </div>
                                        <div class="blognews-info">
                                            <a href="#" class="blognews-cat">Tips & Insights</a>
                                            <h3 class="blognews-title"><a href="#">Create gallery post types as well</a></h3>
                                            <div class="blognews-content"><p>Always strive for better work. Never stop learning. Have fun a clear plan for a new project or just an idea on a napkin?</p></div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-lg-4">
                                    <article class="post">
                                        <div class="blognews-thumb">
                                            <a class="overlay" href="javascript:void(0);">
                                                <img src="images/journal-02.jpg" alt="">
                                                <span class="ion-ios-search-strong"></span>
                                            </a>
                                        </div>
                                        <div class="blognews-info">
                                            <a href="#" class="blognews-cat">Case Studies</a>
                                            <h3 class="blognews-title"><a href="#">Win a Set of TouchPoints Basic</a></h3>
                                            <div class="blognews-content"><p>TouchPoints are neuroscientific wearables that are worn on either side of the body for 15 minutes to reduce stress and anxiety.</p></div>
                                        </div>
                                    </article>
                                </div>
                                <div class="col-lg-4">
                                    <article class="post">
                                        <div class="blognews-thumb">
                                            <a class="overlay" href="javascript:void(0);">
                                                <img src="images/journal-03.jpg" alt="">
                                                <span class="ion-ios-search-strong"></span>
                                            </a>
                                        </div>
                                        <div class="blognews-info">
                                            <a href="#" class="blognews-cat">Products</a>
                                            <h3 class="blognews-title"><a href="#">This Smart Eye Mask Could Be The Answer</a></h3>
                                            <div class="blognews-content"><p>Waking up is hard to do. That’s why this company is making sleep a priority by giving the classic face mask an upgrade.</p></div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .story -->
                --}}
                <div class="partners">
                    <div class="container">
                        <div class="partners-slider owl-carousel">
                            <div><a href="#"><img src="images/partner-01.png" alt=""></a></div>
                            <div><a href="#"><img src="images/partner-02.png" alt=""></a></div>
                            <div><a href="#"><img src="images/partner-03.png" alt=""></a></div>
                            <div><a href="#"><img src="images/partner-04.png" alt=""></a></div>
                            <div><a href="#"><img src="images/partner-05.png" alt=""></a></div>
                            <div><a href="#"><img src="images/partner-06.png" alt=""></a></div>
                        </div>
                    </div>
                </div><!-- .partners -->
            </main><!-- .site-main -->

            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        <!-- jQuery -->    
       
        <script>
            function validate(evt) {
              var theEvent = evt || window.event;
              var key = theEvent.keyCode || theEvent.which;
              key = String.fromCharCode( key );
              var regex = /[0-9]|\./;
              if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
              }
            }
        </script>
    </body>
@endsection