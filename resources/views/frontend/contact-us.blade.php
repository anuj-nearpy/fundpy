@extends('frontend.layouts.app')

@section('content')
    <body class="home home-top">
        <div class="preloading">
            <div class="preloader loading">
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
              <span class="slice"></span>
            </div>
        </div>
        <div id="wrapper">
            @include('frontend.layouts.header-bar')

            <main id="main" class="site-main">
                <div class="page-title background-page">
                    <div class="container">
                        <h1>Say Hello!</h1>
                        <div class="breadcrumbs">
                            <ul>
                                <li><a href="{!! route('frontend.index') !!}">Home</a><span>/</span></li>
                                <li>Contact Us</li>
                            </ul>
                        </div><!-- .breadcrumbs -->
                    </div>
                </div><!-- .page-title -->
                <div class="page-content contact-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 main-content">
                                <div class="entry-content">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-contact">
                                                <h2>Drop US a line</h2>
                                                <form action="{!! route('frontend.inquiry') !!}" method="POST" id="contactForm" class="clearfix">
                                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                                    <div class="clearfix">
                                                        <div class="field align-left">
                                                            <input type="text" value="" name="contact_name" placeholder="Your Name" />
                                                        </div>
                                                        <div class="field align-right">
                                                            <input type="text" value="" name="contact_email" placeholder="Your Email" />
                                                        </div>
                                                    </div>
                                                    <div class="field">
                                                        <input type="text" value="" name="contact_subject" placeholder="Subject" />
                                                    </div>
                                                    <div class="field-textarea">
                                                        <textarea name="contact_message" rows="8" placeholder="Message"></textarea>
                                                    </div>
                                                    <button type="submit" value="Send Messager" class="btn-primary">Submit Message</button>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="contact-info">
                                                <h3>Contact Infomation</h3>
                                                <ul>
                                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i>Doha / Qatar</li>
                                                    <li><i class="fa fa-phone" aria-hidden="true"></i> +(974) 33 43 2827</li>
                                                    <li><i class="fa fa-mobile" aria-hidden="true"></i> +(974) 33 43 2827</li>
                                                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i>info@fundpy.com</li>
                                                </ul>
                                                <div class="contact-desc"><p>IF YOU HAVE ANY QUESTIONS, DO NOT HESITATE TO CONTACT US</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .container -->
                </div><!-- .page-content -->
                <div class="maps">
                    <div id="map">
                    	<iframe style="margin-top:80px;width: 100%;margin-bottom: -5px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d81629.6447639554!2d51.50828543420388!3d25.285560640427338!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e45c534ffdce87f%3A0x44d9319f78cfd4b1!2sDoha%2C+Katar!5e0!3m2!1str!2str!4v1551992066054" width="600" height="560" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div><!-- .maps -->
            </main><!-- .site-main -->

            @include('frontend.layouts.footer')
        </div><!-- #wrapper -->
        <!-- jQuery -->    
       
            <!-- End Google Analytics -->
            <script src="https://maps.googleapis.com/maps/api/js"></script>
            <script>
                // When the window has finished loading create our google map below
                google.maps.event.addDomListener(window, 'load', init);

                function init() {
                    // Basic options for a simple Google Map
                    // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                    var mapOptions = {
                        // How zoomed in you want the map to start at (always required)
                        zoom: 15,
                        scrollwheel: false,
                        // The latitude and longitude to center the map (always required)
                        center: new google.maps.LatLng(51.477286, -0.201811), // New York

                        // How you would like to style the map. 
                        // This is where you would paste any style found on Snazzy Maps.
                        styles: [
                                {
                                    "featureType": "all",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "weight": "2.00"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "all",
                                    "elementType": "geometry.stroke",
                                    "stylers": [
                                        {
                                            "color": "#9c9c9c"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "all",
                                    "elementType": "labels.text",
                                    "stylers": [
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#f2f2f2"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "landscape.man_made",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "poi",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "saturation": -100
                                        },
                                        {
                                            "lightness": 45
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#eeeeee"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#7b7b7b"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.highway",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "simplified"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "road.arterial",
                                    "elementType": "labels.icon",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "transit",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "visibility": "off"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "all",
                                    "stylers": [
                                        {
                                            "color": "#46bcec"
                                        },
                                        {
                                            "visibility": "on"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "geometry.fill",
                                    "stylers": [
                                        {
                                            "color": "#c8d7d4"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text.fill",
                                    "stylers": [
                                        {
                                            "color": "#070707"
                                        }
                                    ]
                                },
                                {
                                    "featureType": "water",
                                    "elementType": "labels.text.stroke",
                                    "stylers": [
                                        {
                                            "color": "#ffffff"
                                        }
                                    ]
                                }
                            ]
                    };
                    // Get the HTML DOM element that will contain your map 
                    // We are using a div with id="map" seen below in the <body>
                    var mapElement = document.getElementById('map');

                    // Create the Google Map using our element and options defined above
                    var map = new google.maps.Map(mapElement, mapOptions);

                    // Let's also add a marker while we're at it
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(18.689100, 105.691398),
                        map: map,
                        title: 'Snazzy!'
                    });
                }
            </script>
    </body>
@endsection